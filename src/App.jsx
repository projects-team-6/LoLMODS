import { Container } from '@mui/material';
import CreateMod from './views/CreateMod/CreateMod';
import Home from './views/Home/Home';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Header from './components/Header/Header';
import Login from './views/Login/Login';
import Register from './views/Register/Register';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from './config/firebase-config';
import { useEffect, useState } from 'react';
import { getUserData } from './services/user.services';
import UserContext from './context/UserContext';
import Notify from './components/Notify/Notify';
import Loading from './components/Loading/Loading';
import Authenticated from './components/Authenticated/Authenticated';
import EditProfile from './views/EditProfile/EditProfile';
import Profile from './views/Profile/Profile';
import ResetPassword from './views/ResetPassword/ResetPassword';
import Admin from './views/Admin/Admin';
import HowToInstall from './views/HowToInstall/HowToInstall';
import SingleMod from './views/SingleMod/SingleMod';
import ScrollToTop from './components/ScrollToTop/ScrollToTop';
import Members from './components/Members/Members';
import Mods from './views/Mods/Mods';
import NotFound from './views/NotFound/NotFound';

function App() {
  const [appState, setAppState] = useState({
    user: null,
    userData: null,
  });

  let [user, loading] = useAuthState(auth);

  const [displayMsg, setDisplayMsg] = useState({ opn: false, msg: '' });

  useEffect(() => {
    if (user === null) return;

    getUserData(user.uid)
      .then((snapshot) => {
        if (!snapshot.exists()) {
          throw new Error('Something went wrong!');
        }
        const result = snapshot.val();
        setAppState({
          user: Object.keys(result)[0],
          userData: {
            ...result[Object.keys(result)[0]],
            mods: result[Object.keys(result)[0]].mods || {},
          },
        });
      })
      .catch((e) => alert(e.message));
  }, [user]);

  return (
    <BrowserRouter>
      <UserContext.Provider value={{ ...appState, setContext: setAppState }}>
        <div className="App">
          <Loading>
            <Header setDisplayMsg={setDisplayMsg} />
            <Container sx={{ padding: '30px 0' }}>
              <Routes>
                <Route index element={<Home />} />
                <Route
                  path="/create-mod"
                  element={
                    <Authenticated loading={loading}>
                      <CreateMod setDisplayMsg={setDisplayMsg} />
                    </Authenticated>
                  }
                />
                <Route path="mods/" element={<Mods setDisplayMsg={setDisplayMsg} />}></Route>
                <Route
                  path="mod/:modId"
                  element={<SingleMod setDisplayMsg={setDisplayMsg} />}
                ></Route>
                <Route
                  path="mod/:modId/members"
                  element={
                    <Authenticated loading={loading}>
                      <Members setDisplayMsg={setDisplayMsg} />
                    </Authenticated>
                  }
                ></Route>
                <Route
                  path="reset"
                  element={<ResetPassword setDisplayMsg={setDisplayMsg} />}
                ></Route>
                <Route path="login" element={<Login setDisplayMsg={setDisplayMsg} />}></Route>
                <Route path="register" element={<Register setDisplayMsg={setDisplayMsg} />}></Route>
                <Route
                  path="edit"
                  element={
                    <Authenticated loading={loading}>
                      <EditProfile setDisplayMsg={setDisplayMsg} />
                    </Authenticated>
                  }
                ></Route>
                <Route
                  path="admin"
                  element={
                    <Authenticated loading={loading}>
                      <Admin setDisplayMsg={setDisplayMsg} />
                    </Authenticated>
                  }
                ></Route>
                <Route
                  path="admin/pending"
                  element={
                    <Authenticated loading={loading}>
                      <Admin setDisplayMsg={setDisplayMsg} view="pending" />
                    </Authenticated>
                  }
                ></Route>
                <Route
                  path="profile/:userName"
                  element={
                    <Authenticated loading={loading}>
                      <Profile setDisplayMsg={setDisplayMsg} />
                    </Authenticated>
                  }
                ></Route>
                <Route
                  path="profile/:userName/pending"
                  element={
                    <Authenticated loading={loading}>
                      <Profile setDisplayMsg={setDisplayMsg} view={'pending'} />
                    </Authenticated>
                  }
                ></Route>
                <Route
                  path="profile/:userName/maintain"
                  element={
                    <Authenticated loading={loading}>
                      <Profile setDisplayMsg={setDisplayMsg} view={'maintain'} />
                    </Authenticated>
                  }
                ></Route>
                <Route path="how-to-install" element={<HowToInstall />}></Route>
                <Route path="*" element={<NotFound opened={true} />} />
              </Routes>
            </Container>
            <Notify opn={displayMsg.opn} msg={displayMsg.msg} setDisplayMsg={setDisplayMsg} />
            <ScrollToTop />
          </Loading>
        </div>
      </UserContext.Provider>
    </BrowserRouter>
  );
}

export default App;
