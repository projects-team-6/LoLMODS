import { createContext } from 'react';

const UserContext = createContext({
  user: null,
  userData: null,
  setContext() {},
});

export default UserContext;
