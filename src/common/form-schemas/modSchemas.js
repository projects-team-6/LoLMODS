import { string, mixed, object } from 'yup';
import {
  MAX_MOD_INFO,
  MAX_MOD_SPECIFICATIONS,
  MAX_MOD_TITLE,
  MAX_MOD_VERSION_MESSAGE,
  MIN_MOD_INFO,
  MIN_MOD_SPECIFICATIONS,
  MIN_MOD_TITLE,
  MIN_MOD_VERSION_MESSAGE,
} from './../constants';

export const modTitleSchema = {
  title: string()
    .trim()
    .min(MIN_MOD_TITLE, `Title must be at least ${MIN_MOD_TITLE} characters`)
    .max(MAX_MOD_TITLE, `Title must be at most ${MAX_MOD_TITLE} characters`)
    .required('Title is required'),
};

export const modMainImageSchema = {
  mainImage: mixed().test('hasMainImage', 'Main Image is required', (value) =>
    value ? true : false
  ),
};

export const modCategoriesSchema = {
  mainCategory: string().required('Categories are required'),
};

export const modInfoSchema = {
  modInfo: string()
    .trim()
    .min(MIN_MOD_INFO, `Mod Info must be at least ${MIN_MOD_INFO} characters`)
    .max(MAX_MOD_INFO, `Mod Info must be at most ${MAX_MOD_INFO} characters`)
    .required('Mod Information is required'),
};

export const modSpecificationsSchema = {
  specifications: string()
    .trim()
    .min(
      MIN_MOD_SPECIFICATIONS,
      `Specifications must be at least ${MIN_MOD_SPECIFICATIONS} characters`
    )
    .max(
      MAX_MOD_SPECIFICATIONS,
      `Specifications must be at least ${MAX_MOD_SPECIFICATIONS} characters`
    )
    .required('Specifications is required'),
};

export const modYoutubeUrlSchema = {
  youtubeURL: string().trim().url('Must be a valid YouTube URL'),
};

export const createModSchema = object({
  ...modTitleSchema,
  ...modMainImageSchema,
  ...modCategoriesSchema,
  ...modInfoSchema,
  ...modSpecificationsSchema,
  ...modYoutubeUrlSchema,
  mod: mixed().test('hasMod', 'Mod is required', (value) => (value ? true : false)),
});

export const editCommentSchema = object({
  comment: string('Enter comment').required('Comment is required'),
});

export const notApprovedSchema = object({
  comment: string().required('Comment is required'),
});

export const updateModSchema = object({
  mod: mixed().test('hasMod', 'Mod is required', (value) => (value ? true : false)),
  versionMessage: string()
    .trim()
    .min(
      MIN_MOD_VERSION_MESSAGE,
      `Version message must be at least ${MIN_MOD_VERSION_MESSAGE} characters`
    )
    .max(
      MAX_MOD_VERSION_MESSAGE,
      `Version message must be at most ${MAX_MOD_VERSION_MESSAGE} characters`
    )
    .required('Version message is required'),
  versionType: string().required('Version type is required'),
});
