// Users
export const MIN_USER_NAME = 2;

export const MAX_USER_NAME = 20;

export const MIN_PASSWORD = 6;

export const MAX_PASSWORD = 30;

export const MIN_FULL_NAME = 2;

export const MAX_FULL_NAME = 40;

export const MIN_SOCIAL_NETWORK = 5;

export const MAX_SOCIAL_NETWORK = 60;

export const PHONE_REGEX =
  /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/;

export const USER_NAME_REGEX = /^[a-zA-Z]{1}[a-zA-Z0-9_]+$/;

export const AVATAR_TYPES = ['image/png', 'image/jpeg'];

// Mods
export const MIN_MOD_TITLE = 5;

export const MAX_MOD_TITLE = 40;

export const MIN_MOD_INFO = 15;

export const MAX_MOD_INFO = 2000;

export const MIN_MOD_SPECIFICATIONS = 15;

export const MAX_MOD_SPECIFICATIONS = 2000;

export const MIN_MOD_VERSION_MESSAGE = 10;

export const MAX_MOD_VERSION_MESSAGE = 40;

// Messages
export const MESSAGE_FOR_APPROVE = 'New mod is waiting for approval!';

export const MESSAGE_APPROVED = 'Your mod was approved!';

export const MESSAGE_DELETED = 'Your mod is deleted!';

// Install page
export const INSTALL_REQUIREMENTS = [
  'At least Windows 8.1',
  'Most recent Windows update',
  'Official Windows license',
  '64-bit processor',
];

export const INSTALL_URL = 'https://github.com/customskinlol/cslol-manager/releases';

export const INSTALL_VIDEO_ID = 'KF9z9TW36mA';
