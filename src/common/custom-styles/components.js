import {
  Autocomplete,
  Button,
  MenuItem,
  Paper,
  Radio,
  Select,
  styled,
  TextField,
} from '@mui/material';
import Filter1Icon from '@mui/icons-material/Filter1';
import Filter2Icon from '@mui/icons-material/Filter2';
import Filter3Icon from '@mui/icons-material/Filter3';
import Filter4Icon from '@mui/icons-material/Filter4';
import Filter5Icon from '@mui/icons-material/Filter5';

export const InitMainTextField = (styles, hoverStyles) =>
  styled(TextField)({
    backgroundColor: 'rgb(189, 189, 189)',
    ...styles,
    '&:hover': {
      outline: '1px solid #181C23',
      ...hoverStyles,
    },
    '& .css-1t8l2tu-MuiInputBase-input-MuiOutlinedInput-input, & .css-1sqnrkk-MuiInputBase-input-MuiOutlinedInput-input':
      {
        fontSize: '20px',
      },
    // input label when focused
    // '& label.Mui-focused': {
    //   color: '#FD0017',
    // },
    // focused color for input with variant='standard'
    '& .MuiInput-underline:after': {
      border: '4px solid #810d0d',
    },
    // focused color for input with variant='filled'
    '& .MuiFilledInput-underline:after': {
      border: '4px solid #810d0d',
    },
    // focused color for input with variant='outlined'
    '& .MuiOutlinedInput-root': {
      '&.Mui-focused fieldset': {
        border: '4px solid #810d0d',
      },
    },
  });

export const InitMainSelect = (styles, hoverStyles) =>
  styled(Select)({
    backgroundColor: 'rgb(189, 189, 189)',
    ...styles,
    '&:hover': {
      outline: '1px solid #181C23',
      ...hoverStyles,
    },
    '& svg': {
      fill: 'black',
    },
    '&.Mui-disabled': {
      backgroundColor: '#000',
    },
    '"&.Mui-focused:after"': {
      outline: '1px solid #181C23',
    },
    '&:after': {
      border: '4px solid #810d0d',
    },
  });

export const InitMainAutocomplete = (styles, hoverStyles) =>
  styled(Autocomplete)({
    backgroundColor: 'rgb(189, 189, 189)',
    ...styles,
    '&:hover': {
      outline: '1px solid #181C23',
      ...hoverStyles,
    },
    '& svg': {
      fill: 'black',
    },
  });

export const InitMainMenuItem = (styles) =>
  styled(MenuItem)({
    color: 'black',
    ...styles,
  });

export const InitMainButton = (styles, hoverStyles) =>
  styled(Button)({
    border: '1px solid #FD0017',
    width: '180px',
    borderRadius: '30px',
    color: '#FD0017',
    padding: '10px 0',
    backgroundColor: 'rgb(22, 22, 22)',
    ...styles,
    '&:hover': {
      backgroundColor: '#000',
      border: '1px solid #FD0017',
      ...hoverStyles,
    },
  });

export const InitRadio = (styles, hoverStyles) =>
  styled(Radio)({
    color: '#FD0017',
    ...styles,
    '& .css-1hbvpl3-MuiSvgIcon-root': {
      fill: '#FD0017',
      ...hoverStyles,
    },
    '& .css-11zohuh-MuiSvgIcon-root': {
      fill: '#FD0017',
      ...hoverStyles,
    },
    '& .css-1hhw7if-MuiSvgIcon-root': {
      fill: '#FD0017',
      ...hoverStyles,
    },
  });

export const CommentItem = (styles) =>
  styled(Paper)({
    textAlign: 'left',
    backgroundColor: 'rgb(189, 189, 189)',
    height: 50,
    width: '100%',
    padding: '5px 10px',
    verticalAlign: 'middle',
    color: '#060606',
    ...styles,
  });

export const lineStyle = { borderBottom: '3px solid red' };

export const iconsStyle = { fill: '#FD0017', marginRight: '8px' };

export const modalStyle = {
  position: 'absolute',
  textAlign: 'center',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 600,
  bgcolor: '#181C23;',
  border: '2px dotted #FD0017',
  boxShadow: 24,
  p: 4,
};

export const uploadImagesIcons = [
  <Filter5Icon sx={iconsStyle} />,
  <Filter4Icon sx={iconsStyle} />,
  <Filter3Icon sx={iconsStyle} />,
  <Filter2Icon sx={iconsStyle} />,
  <Filter1Icon sx={iconsStyle} />,
  null,
];

export const ErrorStyled = ({ children, style }) => (
  <p
    style={{
      color: '#d32f2f',
      textAlign: 'left',
      padding: '10px 0 0 10px',
      fontSize: '15px',
      height: 0,
      ...style,
    }}
  >
    {children}
  </p>
);
