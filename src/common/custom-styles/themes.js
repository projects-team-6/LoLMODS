import white from '@mui/material/colors/grey';
import { createTheme } from '@mui/material/styles';

export const loginRegisterTheme = createTheme({
  palette: {
    primary: {
      main: '#FD0017',
    },
  },
});

export const headerTheme = createTheme({
  palette: {
    primary: {
      main: '#181C23',
    },
    secondary: {
      main: white[50],
    },
  },
});

export const homeTheme = createTheme({
  typography: {
    fontFamily: ['Segoe UI', 'Roboto'].join(','),
    fontWeightRegular: '100',
  },
});

export const categoriesTheme = createTheme({
  palette: {
    primary: {
      main: '#810d0d',
    },
  },
});
