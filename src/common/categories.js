export const categories = {
  Champions: {
    Remodels: {
      'New Skins': null,
      'Model Swaps': null,
      'Champion Swaps': null,
    },
    Retextures: {
      Reskins: null,
      Chromas: null,
      'No Skins': null,
    },
    'Skin Edits': null,
  },
  HUD: {
    'Game Interface': null,
    'Champion Assets': null,
    'Game Assets': null,
  },
  Maps: {
    "Summoner's Rift": null,
    'Howling Abyss': null,
    'Nexus Blitz': null,
    "Old Summoner's Rift": null,
  },
  Sounds: {
    Announcers: null,
    'Champion Voices': null,
    'Ability Sounds': null,
    'Background Music': null,
  },
  Fonts: null,
};
