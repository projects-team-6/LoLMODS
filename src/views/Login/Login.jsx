import { useFormik } from 'formik';
import { Grid, Tabs, Tab, ThemeProvider } from '@mui/material';
import './Login.css';
import { NavLink, useNavigate } from 'react-router-dom';
import { loginUser } from '../../services/auth.service';
import {
  InitMainTextField,
  InitMainButton,
  ErrorStyled,
} from '../../common/custom-styles/components';
import { loginSchema } from '../../common/form-schemas/userSchemas';
import { loginRegisterTheme } from '../../common/custom-styles/themes';

const StyledTextField = InitMainTextField();
const StyledButton = InitMainButton();

const Login = ({ setDisplayMsg }) => {
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: loginSchema,
    onSubmit: (values) => {
      loginUser(values.email, values.password)
        .then(() => {
          setDisplayMsg({ opn: true, msg: 'Log-in successfully!' });
          navigate('/');
        })
        .catch((err) => {
          console.log(err.message);
          if (
            err.message.includes(`wrong-password`) ||
            err.message.includes(`user-not-found`) ||
            err.message.includes(`invalid-email`)
          ) {
            setDisplayMsg({ opn: true, msg: 'Wrong email or password.' });
            return;
          } else if (err.message.includes('auth/too-many-requests')) {
            setDisplayMsg({ opn: true, msg: 'Your account is blocked, please try again later.' });
            return;
          }
        });
      // alert(JSON.stringify(values, null, 2));
    },
  });

  return (
    <ThemeProvider theme={loginRegisterTheme}>
      <div className="auth-container">
        <form onSubmit={formik.handleSubmit}>
          <Grid
            container
            spacing={4}
            direction="column"
            alignItems="center"
            justifyContent="center"
            // style={{ minHeight: '60vh', border: '1px solid' }}
          >
            <Grid item xs={12} className="header-auth">
              <Tabs value={0}>
                <NavLink to={'/login'}>
                  <Tab label="Login" />
                </NavLink>

                <NavLink to={'/register'}>
                  <Tab label="Register" />
                </NavLink>
              </Tabs>
            </Grid>

            <Grid item xs={1}>
              <h4>Email</h4>
              <StyledTextField
                className="login-input"
                id="email"
                name="email"
                // label="Email"
                value={formik.values.email}
                onChange={formik.handleChange}
                error={formik.touched.email && Boolean(formik.errors.email)}
              />
              <ErrorStyled>{formik.touched.email ? formik.errors.email : ''}</ErrorStyled>
            </Grid>

            <Grid item xs={1}>
              <h4>Password</h4>
              <StyledTextField
                className="login-input"
                id="password"
                name="password"
                // label="Password"
                type="password"
                value={formik.values.password}
                onChange={formik.handleChange}
                error={formik.touched.password && Boolean(formik.errors.password)}
              />
              <ErrorStyled>{formik.touched.password ? formik.errors.password : ''}</ErrorStyled>
            </Grid>

            <Grid item xs={1}>
              <StyledButton color="primary" variant="contained" type="submit">
                Login
              </StyledButton>
            </Grid>

            <Grid item xs={1}>
              <NavLink to={'/reset'}>Forgot your password?</NavLink>
            </Grid>
          </Grid>
        </form>
      </div>
    </ThemeProvider>
  );
};

export default Login;
