import { Modal } from '@mui/material';
import { Box } from '@mui/system';
import { useNavigate } from 'react-router-dom';
import { iconsStyle, InitMainButton, modalStyle } from '../../common/custom-styles/components';
import BackspaceIcon from '@mui/icons-material/Backspace';

const StyledButton = InitMainButton({ padding: '10px 10px' });

const NotFound = ({ opened }) => {
  
  const navigate = useNavigate();

  const handelClose = () => {
    navigate(-1);  
  }

  return (
    <Modal open={opened} onClose={handelClose}>
      <Box sx={modalStyle}>
        <h2>404 PAGE NOT FOUND</h2>
        <Box pt="35px">
          <StyledButton onClick={handelClose}>
            <BackspaceIcon sx={iconsStyle}/>
            Go back
          </StyledButton>
        </Box>
      </Box>
    </Modal>
  );
};

export default NotFound;
