import { Avatar, Button, Grid, Stack, TextField } from '@mui/material';
import { useFormik } from 'formik';
import React, { useContext, useState } from 'react';
import {
  ErrorStyled,
  iconsStyle,
  InitMainButton,
  InitMainTextField,
} from '../../common/custom-styles/components';
import UserContext from '../../context/UserContext';
import {
  removeAvatar,
  updateUserData,
  updateUserPassword,
  uploadUserProfilePicture,
} from '../../services/user.services';
import { AVATAR_TYPES } from '../../common/constants';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import CloseIcon from '@mui/icons-material/Close';
import DoneIcon from '@mui/icons-material/Done';
import { changePasswordSchema, editProfileSchema } from '../../common/form-schemas/userSchemas';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

const StyledTextField = InitMainTextField();
const StyledButton = InitMainButton({ width: '220px' });
const StyledButtonRemove = InitMainButton({ width: '220px' });

const EditProfile = ({ setDisplayMsg }) => {
  const { user, userData, setContext } = useContext(UserContext);
  const [avatar, setAvatar] = useState(userData?.avatarUrl || null);
  const [isAvatarChanged, setIsAvatarChanged] = useState(false);

  const info = [
    { name: 'fullName', label: 'Full Name', type: 'text' },
    { name: 'aboutMe', label: 'About Me', type: 'text' },
    { name: 'facebook', label: 'Facebook', type: 'text' },
    { name: 'linkedin', label: 'Linkedin', type: 'text' },
    { name: 'twitter', label: 'Twitter', type: 'text' },
    { name: 'website', label: 'Website link', type: 'text' },
    { name: 'email', label: 'Email', type: 'email' },
    { name: 'phoneNumber', label: 'Phone Number', type: 'text' },
  ];
  const infoPass = [
    { name: 'oldPassword', label: 'Password', type: 'password' },
    { name: 'newPassword', label: 'New Password', type: 'password' },
    { name: 'repeatPassword', label: 'Confirm Password', type: 'password' },
  ];

  const formik = useFormik({
    initialValues: {
      fullName: userData?.fullName || '',
      aboutMe: userData?.aboutMe || '',
      facebook: userData?.facebook || '',
      linkedin: userData?.linkedin || '',
      twitter: userData?.twitter || '',
      website: userData?.website || '',
      email: userData?.email || '',
      phoneNumber: userData?.phoneNumber || '',
    },
    validationSchema: editProfileSchema,
    onSubmit: (values) => {
      const upd = {
        fullName: values.fullName || null,
        aboutMe: values.aboutMe || null,
        facebook: values.facebook || null,
        linkedin: values.linkedin || null,
        twitter: values.twitter || null,
        website: values.website || null,
        email: values.email || null,
        phoneNumber: values.phoneNumber || null,
      };

      updateUserData(user, values.email, upd)
        .then(() => {
          setContext({
            user,
            userData: {
              ...userData,
              ...upd,
            },
          });
          setDisplayMsg({ opn: true, msg: 'Saved!' });
        })
        .catch((e) => {
          if (e.message.includes(`email-already-in-use`)) {
            setDisplayMsg({
              opn: true,
              msg: `Email ${values.email} has already been registered!`,
            });
            return;
          }
        });
    },
  });

  const passFormik = useFormik({
    initialValues: {
      oldPassword: '',
      newPassword: '',
      repeatPassword: '',
    },
    validationSchema: changePasswordSchema,
    onSubmit: (values) => {
      updateUserPassword(values.oldPassword, values.newPassword)
        .then(() => {
          passFormik.resetForm();
          setDisplayMsg({ opn: true, msg: 'Password changed successfully!' });
        })
        .catch(console.log);
    },
  });

  const handleAvatarChange = (e) => {
    const file = e.target.files?.[0];
    if (!AVATAR_TYPES.includes(file.type)) {
      e.target.form[0].value = null;
      setIsAvatarChanged(false);
      setAvatar(userData?.avatarUrl);
      return setDisplayMsg({ opn: true, msg: 'Unsupported image format!' });
    }
    const avatarUrl = URL.createObjectURL(file);
    setIsAvatarChanged(true);
    setAvatar(avatarUrl);
  };

  const handelAvatarSave = (e) => {
    e.preventDefault();

    const file = e.target[0].files?.[0];
    e.target[0].value = null;
    uploadUserProfilePicture(user, file)
      .then((url) => {
        setContext({
          user,
          userData: {
            ...userData,
            avatarUrl: url,
          },
        });
        setIsAvatarChanged(false);
        setAvatar(url);
        setDisplayMsg({ opn: true, msg: 'Avatar saved' });
      })
      .catch(console.log);
  };

  const handelAvatarCancel = (e) => {
    e.currentTarget.form[0].value = null;
    setIsAvatarChanged(false);
    setAvatar(userData?.avatarUrl);
    setDisplayMsg({ opn: true, msg: 'Avatar removed' });
  };

  const handelAvatarRemove = (e) => {
    removeAvatar(user)
      .then(() => {
        e.target.form[0].value = null;
        setIsAvatarChanged(false);
        setAvatar(null);
        setContext({
          user,
          userData: {
            ...userData,
            avatarUrl: null,
          },
        });
        setDisplayMsg({ opn: true, msg: 'Avatar removed' });
      })
      .catch(console.log);
  };

  return (
    <Grid
      container
      alignItems="flex-start"
      justifyContent="center"
      spacing={4}
      rowSpacing={4}
      columnSpacing={{ xs: 1, sm: 2, md: 3 }}
      sx={{ pb: 5, borderBottom: '1px solid' }}
    >
      <Grid item xs={12} textAlign="center">
        <h1>Edit Profile</h1>
      </Grid>
      <Grid item style={{ borderBottom: '1px solid', paddingBottom: '10px' }} xs={12}></Grid>
      <Grid item xs={4} style={{ textAlign: 'left' }}>
        <h2>Public avatar </h2>
        <p>You can change your avatar here or remove the current avatar.</p>
      </Grid>
      <Grid item xs={1} style={{ alignItems: 'right' }}>
        <Avatar alt="avatar" src={avatar} sx={{ width: 100, height: 100, fontSize: 40 }}>
          {!avatar ? user[0].toUpperCase() : null}
        </Avatar>
      </Grid>
      <Grid item xs={7} style={{ paddingTop: '30px', paddingLeft: '40px' }}>
        <form onSubmit={handelAvatarSave}>
          <Stack rowGap={2}>
            <Stack direction="row">
              <label>
                <TextField onChange={handleAvatarChange} variant="outlined" type="file" />
                <StyledButton component="span">
                  <AccountCircleOutlinedIcon sx={iconsStyle} />
                  Upload Avatar
                </StyledButton>
              </label>
              {isAvatarChanged ? (
                <>
                  <Button variant="text" color="inherit" type="submit">
                    <DoneIcon />
                  </Button>
                  <Button variant="text" color="inherit" onClick={handelAvatarCancel}>
                    <CloseIcon />
                  </Button>
                </>
              ) : null}
            </Stack>
            <Stack>
              {userData?.avatarUrl ? (
                <StyledButtonRemove variant="text" onClick={handelAvatarRemove}>
                  <DeleteForeverIcon sx={iconsStyle} />
                  Remove avatar
                </StyledButtonRemove>
              ) : null}
            </Stack>
          </Stack>
        </form>
      </Grid>
      <Grid
        item
        style={{
          borderBottom: '1px solid',
          paddingBottom: '10px',
          paddingTop: '40px',
        }}
        xs={12}
      ></Grid>

      <Grid item xs={4}>
        <h2>Main settings </h2>
        <p>This information will appear on your profile</p>
      </Grid>

      <Grid item xs={8}>
        <form onSubmit={formik.handleSubmit}>
          <Grid container rowSpacing={4}>
            {info.map((info, i) => {
              return (
                <Grid item key={i} xs={12}>
                  <h3>{info.label}</h3>
                  <StyledTextField
                    fullWidth
                    type={info.type}
                    id={info.name}
                    name={info.name}
                    value={formik.values[info.name]}
                    onChange={formik.handleChange}
                    error={formik.touched[info.name] && Boolean(formik.errors[info.name])}
                  />
                  <ErrorStyled>
                    {formik.touched[info.name] ? formik.errors[info.name] : ''}
                  </ErrorStyled>
                </Grid>
              );
            })}
            <Grid item xs={12}>
              <StyledButton type="submit">Save Changes</StyledButton>
            </Grid>
          </Grid>
        </form>
      </Grid>
      <Grid
        item
        style={{
          borderBottom: '1px solid',
          paddingBottom: '10px',
          paddingTop: '40px',
        }}
        xs={12}
      ></Grid>
      <Grid item xs={4}>
        <h2>Password</h2>
        <p>You can change your password</p>
      </Grid>
      <Grid item xs={8}>
        <form onSubmit={passFormik.handleSubmit}>
          <Grid container rowSpacing={4}>
            {infoPass.map((info, i) => {
              return (
                <React.Fragment key={i}>
                  <Grid item xs={12}>
                    <h3>{info.label}</h3>
                    <StyledTextField
                      fullWidth
                      type={info.type}
                      id={info.name}
                      name={info.name}
                      value={passFormik.values[info.name]}
                      onChange={passFormik.handleChange}
                      error={passFormik.touched[info.name] && Boolean(passFormik.errors[info.name])}
                    />
                  </Grid>
                  <ErrorStyled>
                    {passFormik.touched[info.name] ? passFormik.errors[info.name] : ''}
                  </ErrorStyled>
                </React.Fragment>
              );
            })}
            <Grid item xs={12}>
              <StyledButton color="success" variant="contained" type="submit">
                Change Password
              </StyledButton>
            </Grid>
          </Grid>
        </form>
      </Grid>
    </Grid>
  );
};

export default EditProfile;
