import './Home.scss';
import { Grid, Typography, ThemeProvider } from '@mui/material';
import { useEffect, useState } from 'react';
import ModCard from '../../components/ModCard/ModCard';
import { getApprovedMods } from '../../services/mod.services';
import { lineStyle } from '../../common/custom-styles/components';
import ArrowCircleRightOutlinedIcon from '@mui/icons-material/ArrowCircleRightOutlined';
import { NavLink } from 'react-router-dom';
import { homeTheme } from '../../common/custom-styles/themes';

export default function Home() {
  const [mods, setMods] = useState(null);
  const [mostPopularMods, setMostPopularMods] = useState([]);
  const [highestRatedMods, setHighestRatedMods] = useState([]);
  const [recentMods, setRecentMods] = useState([]);

  useEffect(() => {
    (async () => {
      try {
        const modsSnapshot = await getApprovedMods();
        let mods = modsSnapshot.exists() ? Object.entries(modsSnapshot.val()) : [];
        mods = mods.map(([id, mod]) => {
          const rating = mod.rating
            ? Object.values(mod.rating).reduce((acc, r) => acc + r, 0) /
              Object.keys(mod.rating).length
            : 0;
          const downloads = mod.downloads ? mod.downloads : 0;
          return [id, { ...mod, rating, downloads }];
        });

        setMostPopularMods(
          [...mods].sort(([_, a], [__, b]) => b.downloads - a.downloads).slice(0, 6)
        );
        setHighestRatedMods([...mods].sort(([_, a], [__, b]) => b.rating - a.rating).slice(0, 6));
        setRecentMods([...mods].sort(([_, a], [__, b]) => b.createdOn - a.createdOn).slice(0, 6));
        setMods(true);
      } catch (e) {
        console.error(e);
      }
    })();
  }, []);

  return (
    <ThemeProvider theme={homeTheme}>
      <Typography
        variant="h3"
        component="h1"
        letterSpacing="2px"
        textAlign="center"
        marginTop="50px"
        marginBottom="100px"
      >
        Your favorite custom mods for League of Legends
      </Typography>

      {mods ? (
        <Grid container direction="column" rowSpacing={6}>
          <Grid item container spacing={2} direction="column">
            <Grid item xs={12} container alignItems="end" justifyContent="space-between">
              <Typography variant="h4" component="h1" letterSpacing="2px">
                Most Popular
              </Typography>
              <NavLink
                to="/mods?searchParam=&mainCat=All&subCat=All&subSubCat=All&sort=Downloads"
                className="home-see-more-wrapper"
              >
                <h3>See more</h3>
                <ArrowCircleRightOutlinedIcon />
              </NavLink>
            </Grid>
            <Grid item xs={12}>
              <div style={lineStyle} />
            </Grid>
            <Grid item xs={12} container spacing={2}>
              {mostPopularMods.map(([id, mod]) => {
                return <ModCard mod={{ ...mod, id }} key={id} />;
              })}
            </Grid>
            <Grid />
          </Grid>

          <Grid item container spacing={2} direction="column">
            <Grid item xs={12} container alignItems="end" justifyContent="space-between">
              <Typography variant="h4" component="h1" letterSpacing="2px">
                Highest Rated
              </Typography>
              <NavLink
                to="/mods?searchParam=&mainCat=All&subCat=All&subSubCat=All&sort=Rating"
                className="home-see-more-wrapper"
              >
                <h3>See more</h3>
                <ArrowCircleRightOutlinedIcon />
              </NavLink>
            </Grid>
            <Grid item xs={12}>
              <div style={lineStyle} />
            </Grid>
            <Grid item xs={12} container spacing={2}>
              {highestRatedMods.map(([id, mod]) => {
                return <ModCard mod={{ ...mod, id }} key={id} />;
              })}
            </Grid>
            <Grid />
          </Grid>

          <Grid item container spacing={2} direction="column">
            <Grid item xs={12} container alignItems="end" justifyContent="space-between">
              <Typography variant="h4" component="h1" letterSpacing="2px">
                Recent
              </Typography>
              <NavLink
                to="/mods?searchParam=&mainCat=All&subCat=All&subSubCat=All&sort=New"
                className="home-see-more-wrapper"
              >
                <h3>See more</h3>
                <ArrowCircleRightOutlinedIcon />
              </NavLink>
            </Grid>
            <Grid item xs={12}>
              <div style={lineStyle} />
            </Grid>
            <Grid item xs={12} container spacing={2}>
              {recentMods.map(([id, mod]) => {
                return <ModCard mod={{ ...mod, id }} key={id} />;
              })}
            </Grid>
            <Grid />
          </Grid>
        </Grid>
      ) : null}
    </ThemeProvider>
  );
}
