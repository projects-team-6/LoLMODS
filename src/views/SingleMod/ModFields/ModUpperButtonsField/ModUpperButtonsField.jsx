import { Grid } from '@mui/material';
import { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { iconsStyle } from '../../../../common/custom-styles/components';
import UserContext from '../../../../context/UserContext';
import EditCategoriesModal from '../../ModModals/EditCategoriesModal/EditCategoriesModal';
import EditMainImageModal from '../../ModModals/EditMainImageModal/EditMainImageModal';
import EditTitleModal from '../../ModModals/EditTitleModal/EditTitleModal';
import EditIcon from '@mui/icons-material/Edit';
import PeopleIcon from '@mui/icons-material/People';

export default function ModUpperButtonsField({
  modId,
  currentMod,
  setCurrentMod,
  modals,
  setModals,
  setDisplayMsg,
  StyledButton,
}) {
  const { userData } = useContext(UserContext);
  const navigate = useNavigate();

  return (
    <>
      {userData?.uid === currentMod?.author?.uid || userData?.admin || userData?.mods[modId] ? (
        <Grid item xs={12} container columnSpacing={2}>
          {/* Edit Title */}
          <Grid item>
            <StyledButton onClick={() => setModals({ ...modals, editTitle: true })}>
              <EditIcon sx={iconsStyle} />
              Edit Title
            </StyledButton>
          </Grid>
          {modals.editTitle && (
            <EditTitleModal
              mod={currentMod}
              setMod={setCurrentMod}
              opened={modals.editTitle}
              handleClose={() => setModals({ ...modals, editTitle: false })}
              setDisplayMsg={setDisplayMsg}
            />
          )}
          {/* Edit Main Image */}
          <Grid item>
            <StyledButton onClick={() => setModals({ ...modals, editMainImage: true })}>
              <EditIcon sx={iconsStyle} />
              Edit Main Image
            </StyledButton>
          </Grid>
          {modals.editMainImage && (
            <EditMainImageModal
              mod={currentMod}
              imageName={currentMod.mainImage.split('main-image%2F')[1].split('?')[0]}
              setMod={setCurrentMod}
              opened={modals.editMainImage}
              handleClose={() => setModals({ ...modals, editMainImage: false })}
              setDisplayMsg={setDisplayMsg}
            />
          )}
          {/* Edit Categories */}
          <Grid item>
            <StyledButton onClick={() => setModals({ ...modals, editCategories: true })}>
              <EditIcon sx={iconsStyle} />
              Edit Categories
            </StyledButton>
          </Grid>
          {modals.editCategories && (
            <EditCategoriesModal
              mod={currentMod}
              setMod={setCurrentMod}
              opened={modals.editCategories}
              handleClose={() => setModals({ ...modals, editCategories: false })}
              setDisplayMsg={setDisplayMsg}
            />
          )}
          {/* Members */}
          {currentMod?.approved &&
          (userData?.uid === currentMod?.author?.uid || userData?.mods[modId]) ? (
            <Grid item sx={{ marginLeft: 'auto' }}>
              <StyledButton onClick={() => navigate(`/mod/${modId}/members`)}>
                <PeopleIcon sx={iconsStyle} />
                Members
              </StyledButton>
            </Grid>
          ) : null}
        </Grid>
      ) : null}
    </>
  );
}
