import { Grid, Stack } from '@mui/material';
import { useContext } from 'react';
import { iconsStyle, lineStyle } from '../../../../common/custom-styles/components';
import UserContext from '../../../../context/UserContext';
import EditIcon from '@mui/icons-material/Edit';
import EditModInformation from '../../ModModals/EditModInformation/EditModInformation';

export default function ModInformationField({
  modId,
  currentMod,
  setCurrentMod,
  modals,
  setModals,
  StyledButton,
  setDisplayMsg,
}) {
  const { userData } = useContext(UserContext);

  return (
    <>
      <Grid item style={lineStyle} xs={12}></Grid>
      <Grid item xs={12}>
        <Stack direction="row" justifyContent="space-between" alignItems="center">
          <h2>MOD INFORMATION</h2>
          {userData?.uid === currentMod?.author?.uid || userData?.admin || userData?.mods[modId] ? (
            <StyledButton onClick={() => setModals({ ...modals, editModInfo: true })}>
              <EditIcon sx={iconsStyle} />
              Edit Mod Info
            </StyledButton>
          ) : null}
        </Stack>

        {modals.editModInfo && (
          <EditModInformation
            mod={currentMod}
            setMod={setCurrentMod}
            opened={modals.editModInfo}
            handleClose={() => setModals({ ...modals, editModInfo: false })}
            setDisplayMsg={setDisplayMsg}
          />
        )}
      </Grid>

      <Grid item xs={12}>
        <ul className="info-and-specs">
          {currentMod?.modInfo?.map((r, i) => (
            <li key={i}>{r}</li>
          ))}
        </ul>
      </Grid>
    </>
  );
}
