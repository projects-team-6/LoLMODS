import { Stack, Typography, Grid } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { lineStyle } from '../../../../common/custom-styles/components';

export default function ModInstallationField({ DownloadButton }) {
  const navigate = useNavigate();

  return (
    <>
      <Grid item style={lineStyle} xs={12}></Grid>
      <Grid item xs={12}>
        <h2>INSTALLATION</h2>
      </Grid>
      <Grid item xs={12} textAlign="center">
        <Grid container direction="row" rowSpacing={1}>
          <Grid item xs={6} container justifyContent="center">
            <Stack direction="column" rowGap={3}>
              <Typography component="p">
                This tutorial works with ALL of the mods you can find on this website.
              </Typography>
              <DownloadButton
                className="install-button"
                variant="outlined"
                onClick={() => navigate('/how-to-install')}
              >
                Install Mods with custom LoL Custom Skin Manager
              </DownloadButton>
            </Stack>
          </Grid>
          <Grid item xs={6}>
            <img className="main-img" src={process.env.PUBLIC_URL + '/install.png'} alt="img" />
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
