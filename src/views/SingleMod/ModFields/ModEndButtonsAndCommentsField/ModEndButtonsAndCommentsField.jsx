import { Grid } from '@mui/material';
import { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { iconsStyle } from '../../../../common/custom-styles/components';
import Comments from '../../../../components/Comments/Comments';
import UserContext from '../../../../context/UserContext';
import { addMessage } from '../../../../services/message.service';
import { deleteModById, updateModField } from '../../../../services/mod.services';
import NotApprovedModal from '../../ModModals/NotApprovedModal/NotApprovedModal';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

export default function ModEndButtonsAndCommentsField({
  modId,
  currentMod,
  setCurrentMod,
  modals,
  setModals,
  StyledButton,
  setDisplayMsg,
}) {
  const { userData } = useContext(UserContext);

  const navigate = useNavigate();

  const handleApprove = async () => {
    await updateModField(modId, 'approved', true);
    await updateModField(modId, 'createdOn', Number(new Date()));

    setCurrentMod({ ...currentMod, approved: true, createdOn: new Date(currentMod.createdOn) });
    setDisplayMsg({ opn: true, msg: 'The mode is approved successfully' });

    addMessage('Your mod was approved', userData.username, modId, currentMod.author.username);
  };

  const deleteMod = async () => {
    try {
      await deleteModById(currentMod, userData.username);
      setDisplayMsg({ opn: true, msg: 'Successfully deleted.' });
      navigate(`/profile/${userData.username}`, { replace: true });
    } catch (e) {
      setDisplayMsg({ opn: true, msg: 'Error ocurred, please try again later.' });
      console.error(e);
    }
  };

  return (
    <>
      <Grid item xs={12} container justifyContent="center" rowSpacing={3} columnSpacing={2}>
        {!currentMod?.approved && userData?.admin ? (
          <>
            <Grid item>
              <StyledButton onClick={handleApprove}>Approve</StyledButton>
            </Grid>
            <Grid item>
              <StyledButton onClick={() => setModals({ ...modals, notApproved: true })}>
                Not Approve
              </StyledButton>
            </Grid>
            <NotApprovedModal
              mod={currentMod}
              opened={modals.notApproved}
              handleClose={() => setModals({ ...modals, notApproved: false })}
            />
          </>
        ) : (
          <>
            {userData?.uid === currentMod?.author?.uid ? (
              <Grid item>
                <StyledButton onClick={deleteMod}>
                  <DeleteForeverIcon sx={iconsStyle} />
                  Delete mod
                </StyledButton>
              </Grid>
            ) : (
              userData?.admin && (
                <>
                  <Grid item>
                    <StyledButton onClick={() => setModals({ ...modals, notApproved: true })}>
                      <DeleteForeverIcon sx={iconsStyle} />
                      Delete mod
                    </StyledButton>
                  </Grid>
                  <NotApprovedModal
                    mod={currentMod}
                    opened={modals.notApproved}
                    handleClose={() => setModals({ ...modals, notApproved: false })}
                  />
                </>
              )
            )}
            {currentMod?.approved && (
              <>
                <Grid item xs={12}>
                  <h2>COMMENTS</h2>
                </Grid>
                <Grid item xs={12}>
                  <Comments modId={modId} setDisplayMsg={setDisplayMsg} />
                </Grid>
              </>
            )}
          </>
        )}
      </Grid>
    </>
  );
}
