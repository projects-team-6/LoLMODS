import { Avatar, Chip, Stack, Typography, Rating } from '@mui/material';
import { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import UserContext from '../../../../context/UserContext';
import { updateModField } from '../../../../services/mod.services';
import { updateUserField } from '../../../../services/user.services';

export default function ModDetailsField({ currentMod, modCategories, setDisplayMsg, modId }) {
  const { userData, setContext } = useContext(UserContext);
  const navigate = useNavigate();

  const handelRating = (_, rating) => {
    if (!userData) return setDisplayMsg({ opn: true, msg: 'Log in first!' });

    if (userData.uid === currentMod.author.uid)
      return setDisplayMsg({ opn: true, msg: 'Rating your own mod? Nah, mate :).' });

    updateModField(modId, `rating/${userData.uid}`, rating)
      .then(() => {
        return updateUserField(userData.username, `ratings/${modId}`, rating);
      })
      .then(() => {
        setContext({
          user: userData.username,
          userData: { ...userData, ratings: { ...userData.ratings, [modId]: rating } },
        });
      });
  };

  const handelCategoryClick = (e) => {
    const target = e.target.innerText;
    navigate(
      `/mods?searchParam=&mainCat=${modCategories[0]}&subCat=${
        target === modCategories[0] ? 'All' : modCategories[1]
      }&subSubCat=${
        target === modCategories[0] || target === modCategories[1] ? 'All' : modCategories[2]
      }&sort=New`
    );
  };

  return (
    <>
      <Stack direction="row" columnGap={1} sx={{ marginTop: '15px' }}>
        <Avatar alt="avatar" src={currentMod?.author?.avatarUrl} sx={{ width: 25, height: 25 }}>
          {!currentMod?.author?.avatarUrl ? currentMod?.author?.username[0].toUpperCase() : null}
        </Avatar>
        by{' '}
        <span
          className="username-link"
          onClick={() => navigate(`/profile/${currentMod?.author?.username}`)}
        >{`${currentMod?.author?.username} `}</span>
        <Typography component="span" sx={{ flexGrow: 1 }}>
          {new Date(currentMod?.createdOn).toLocaleDateString('en-GB', {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric',
          })}
        </Typography>
        <Stack direction="row" spacing={1}>
          <Chip
            label={currentMod?.mainCategory}
            sx={{ backgroundColor: 'red' }}
            onClick={handelCategoryClick}
          />
          {currentMod?.subCategory && (
            <Chip
              label={currentMod?.subCategory}
              sx={{ backgroundColor: '#911010' }}
              onClick={handelCategoryClick}
            />
          )}
          {currentMod?.subSubCategory && (
            <Chip
              label={currentMod?.subSubCategory}
              sx={{ backgroundColor: '#680d0d' }}
              onClick={handelCategoryClick}
            />
          )}
        </Stack>
      </Stack>
      <Stack direction="row" sx={{ marginTop: '15px' }}>
        <Typography
          component="span"
          sx={{ flexGrow: 1 }}
        >{`Downloads: ${currentMod?.downloads}`}</Typography>
        <Rating name="rating" value={+currentMod?.rating} precision={0.5} onChange={handelRating} />
        (
        {`${currentMod?.rating?.toFixed(2)} from ${currentMod?.totalNumberRatings} ${
          currentMod?.totalNumberRatings === 1 ? 'vote' : 'votes'
        }`}
        )
      </Stack>
    </>
  );
}
