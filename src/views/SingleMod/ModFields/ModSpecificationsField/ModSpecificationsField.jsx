import { Grid, Stack } from '@mui/material';
import { useContext } from 'react';
import { iconsStyle, lineStyle } from '../../../../common/custom-styles/components';
import UserContext from '../../../../context/UserContext';
import EditIcon from '@mui/icons-material/Edit';
import EditModSpecificationsModal from '../../ModModals/EditSpecificationsModal/EditSpecificationsModal';

export default function ModSpecificationsField({
  modId,
  currentMod,
  setCurrentMod,
  modals,
  setModals,
  StyledButton,
  setDisplayMsg,
}) {
  const { userData } = useContext(UserContext);

  return (
    <>
      <Grid item style={lineStyle} xs={12}></Grid>
      <Grid item xs={12}>
        <Stack direction="row" justifyContent="space-between" alignItems="center">
          <h2>SPECIFICATIONS</h2>
          {userData?.uid === currentMod?.author?.uid || userData?.admin || userData?.mods[modId] ? (
            <StyledButton onClick={() => setModals({ ...modals, editSpecifications: true })}>
              <EditIcon sx={iconsStyle} />
              Edit Specifications
            </StyledButton>
          ) : null}
        </Stack>

        {modals.editSpecifications && (
          <EditModSpecificationsModal
            mod={currentMod}
            setMod={setCurrentMod}
            opened={modals.editSpecifications}
            handleClose={() => setModals({ ...modals, editSpecifications: false })}
            setDisplayMsg={setDisplayMsg}
          />
        )}
      </Grid>
      <Grid item xs={12}>
        <ul className="info-and-specs">
          {currentMod?.specifications?.map((r, i) => (
            <li key={i}>{r}</li>
          ))}
        </ul>
      </Grid>
    </>
  );
}
