import { Grid, Stack } from '@mui/material';
import { useContext } from 'react';
import { iconsStyle, lineStyle } from '../../../../common/custom-styles/components';
import UserContext from '../../../../context/UserContext';
import EditIcon from '@mui/icons-material/Edit';
import EditPreviewModal from '../../ModModals/EditPreviewModal/EditPreviewModal';
import YoutubeEmbed from '../../../../components/YoutubeEmbed/YoutubeEmbed';

export default function ModPreviewField({
  modId,
  currentMod,
  setCurrentMod,
  modals,
  setModals,
  StyledButton,
  setDisplayMsg,
}) {
  const { userData } = useContext(UserContext);

  return (
    <>
      {currentMod?.youtubeURL ||
      currentMod?.images ||
      userData?.admin ||
      userData?.uid === currentMod?.author?.uid ? (
        <>
          <Grid item style={lineStyle} xs={12}></Grid>
          <Grid item xs={12}>
            <Stack direction="row" justifyContent="space-between" alignItems="center">
              <h2>PREVIEW</h2>
              {userData?.uid === currentMod?.author?.uid ||
              userData?.admin ||
              userData?.mods[modId] ? (
                <StyledButton onClick={() => setModals({ ...modals, editPreview: true })}>
                  <EditIcon sx={iconsStyle} />
                  Edit Preview
                </StyledButton>
              ) : null}
            </Stack>

            {modals.editPreview && (
              <EditPreviewModal
                mod={currentMod}
                setMod={setCurrentMod}
                opened={modals.editPreview}
                handleClose={() => setModals({ ...modals, editPreview: false })}
                setDisplayMsg={setDisplayMsg}
              />
            )}
          </Grid>
          <Grid item xs={12} textAlign="center">
            <Grid container direction="row" rowSpacing={1}>
              {currentMod?.images?.map((img, i) => {
                return (
                  <Grid item xs={6} key={i}>
                    <img className="preview-img" src={img} alt="img" />
                  </Grid>
                );
              })}
            </Grid>
            {currentMod?.youtubeURL !== null ? (
              <YoutubeEmbed embedId={currentMod?.youtubeURL} />
            ) : null}
          </Grid>
        </>
      ) : null}
    </>
  );
}
