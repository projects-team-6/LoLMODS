import { Grid, Modal } from '@mui/material';
import {
  modalStyle,
  InitMainButton,
} from '../../../../common/custom-styles/components';
import { useFormik } from 'formik';
import { Box } from '@mui/system';
import { object } from 'yup';
import { modMainImageSchema } from '../../../../common/form-schemas/modSchemas';
import {
  addModImageBinaryDataToStorage,
  deleteMainImageByModId,
  updateModField,
} from '../../../../services/mod.services';
import { useContext, useState } from 'react';
import UserContext from '../../../../context/UserContext';
import Loading from '../../../../components/Loading/Loading';
import FormFieldMainImage from '../../../CreateMod/FormFieldMainImage/FormFieldMainImage';

const StyledButton = InitMainButton({ padding: '10px 10px' });

const EditMainImageModal = ({ imageName, mod, setMod, opened, handleClose, setDisplayMsg }) => {
  const [submitting, setSubmitting] = useState(false);
  const { user } = useContext(UserContext);

  const formik = useFormik({
    initialValues: {
      mainImage: { name: imageName },
    },
    validationSchema: object(modMainImageSchema),
    onSubmit: async (values) => {
      if (imageName === values.mainImage.name) {
        setDisplayMsg({ opn: true, msg: 'Main Image must be different.' });
        return;
      }

      setSubmitting(true);
      setDisplayMsg({ opn: true, msg: 'Main image is uploading' });

      try {
        await deleteMainImageByModId(mod);
        const imageUrl = await addModImageBinaryDataToStorage(
          values.mainImage,
          mod.id,
          'main-image'
        );
        await updateModField(mod.id, 'mainImage', imageUrl);
        setMod({ ...mod, mainImage: imageUrl });

        await updateModField(mod.id, `lastActivity/${user}`, Number(new Date()));
        setDisplayMsg({ opn: true, msg: 'Main Image updated.' });
        handleClose();
      } catch (e) {
        console.error(e);
        setDisplayMsg({ opn: true, msg: 'Error ocurred, please try again later.' });
      }
    },
  });

  return (
    <>
      {submitting && <Loading infinite />}
      <Modal open={opened} onClose={handleClose}>
        <form onSubmit={formik.handleSubmit}>
          <Box sx={modalStyle}>
            <h2 style={{ paddingBottom: '35px' }}>Edit main image</h2>

            <Grid container spacing={1}>
              <FormFieldMainImage formik={formik} StyledButton={StyledButton} />
            </Grid>

            <Box>
              <StyledButton type="submit">Save</StyledButton>
            </Box>
          </Box>
        </form>
      </Modal>
    </>
  );
};

export default EditMainImageModal;
