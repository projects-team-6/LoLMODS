import { Modal } from '@mui/material';
import {
  modalStyle,
  ErrorStyled,
  InitMainButton,
  InitMainTextField,
} from '../../../../common/custom-styles/components';
import { useFormik } from 'formik';
import { Box } from '@mui/system';
import { notApprovedSchema } from '../../../../common/form-schemas/modSchemas';
import { useNavigate } from 'react-router-dom';
import { addMessage } from '../../../../services/message.service';
import { useContext } from 'react';
import UserContext from '../../../../context/UserContext';
import { deleteModById, deleteModFileById, updateModField } from '../../../../services/mod.services';

const StyledButton = InitMainButton({ padding: '10px 10px' });
const StyledTextField = InitMainTextField({ width: '100%' });

const NotApprovedModal = ({ mod, setMod, opened, handleClose, type = 'delete' }) => {
  const { userData } = useContext(UserContext);

  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      comment: '',
    },
    validationSchema: notApprovedSchema,
    onSubmit: async (values) => {
      if (type === 'delete') {
        try {
          await deleteModById(mod, userData.username);

          addMessage(
            `"${mod.title}" was removed: ${values.comment}`,
            userData.username,
            null,
            mod.author.username
          );

          navigate(-1);
        } catch (e) {
          console.error(e);
        }
      } else if (type === 'update') {
        try {
          await deleteModFileById(mod.id, mod.newVersion.mod.split('token=')[1]);
          await updateModField(mod.id, 'newVersion', null);

          addMessage(
            `"${mod.title}" version update was removed: ${values.comment}`,
            userData.username,
            null,
            mod.author.username
          );
          setMod({ ...mod, newVersion: null });
          handleClose();
        } catch (e) {
          console.error(e);
        }
      }
    },
  });

  return (
    <Modal open={opened} onClose={handleClose}>
      <form onSubmit={formik.handleSubmit}>
        <Box sx={modalStyle}>
          <h2 style={{ paddingBottom: '35px' }}>Comment</h2>
          <StyledTextField
            multiline
            id="comment"
            name="comment"
            autoComplete="off"
            rows={4}
            value={formik.values.comment}
            onChange={formik.handleChange}
            error={formik.touched.comment && Boolean(formik.errors.comment)}
          />
          <ErrorStyled>{formik.errors.comment}</ErrorStyled>
          <Box pt="35px">
            <StyledButton type="submit">Send</StyledButton>
          </Box>
        </Box>
      </form>
    </Modal>
  );
};

export default NotApprovedModal;
