import { Modal } from '@mui/material';
import {
  modalStyle,
  ErrorStyled,
  InitMainButton,
  InitMainTextField,
} from '../../../../common/custom-styles/components';
import { useFormik } from 'formik';
import { Box } from '@mui/system';
import { object } from 'yup';
import { modTitleSchema } from '../../../../common/form-schemas/modSchemas';
import { updateModField } from '../../../../services/mod.services';
import { useContext } from 'react';
import UserContext from '../../../../context/UserContext';

const StyledButton = InitMainButton({ padding: '10px 10px' });
const StyledTextField = InitMainTextField({ width: '100%' });

const EditTitleModal = ({ mod, setMod, opened, handleClose, setDisplayMsg }) => {
  const { user } = useContext(UserContext);

  const formik = useFormik({
    initialValues: {
      title: mod.title,
    },
    validationSchema: object(modTitleSchema),
    onSubmit: async (values) => {
      const trimmedTitle = values.title.trim();

      if (trimmedTitle === mod.title)
        return formik.setFieldError('title', 'Title must be different');

      try {
        await updateModField(mod.id, 'title', trimmedTitle);
        await updateModField(mod.id, `lastActivity/${user}`, Number(new Date()));
        setMod({ ...mod, title: trimmedTitle });
        setDisplayMsg({ opn: true, msg: 'Title updated.' });
        handleClose();
      } catch (e) {
        console.error(e);
      }
    },
  });

  return (
    <Modal open={opened} onClose={handleClose}>
      <form onSubmit={formik.handleSubmit}>
        <Box sx={modalStyle}>
          <h2 style={{ paddingBottom: '35px' }}>Edit title</h2>
          <StyledTextField
            {...formik.getFieldProps('title')}
            id="title"
            value={formik.values.title}
            error={formik.touched.title && Boolean(formik.errors.title)}
          />
          <ErrorStyled>{formik.errors.title}</ErrorStyled>
          <Box pt="35px">
            <StyledButton type="submit">Save</StyledButton>
          </Box>
        </Box>
      </form>
    </Modal>
  );
};

export default EditTitleModal;
