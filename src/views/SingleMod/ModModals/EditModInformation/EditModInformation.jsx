import { Modal } from '@mui/material';
import {
  modalStyle,
  ErrorStyled,
  InitMainButton,
  InitMainTextField,
} from '../../../../common/custom-styles/components';
import { useFormik } from 'formik';
import { Box } from '@mui/system';
import { object } from 'yup';
import { modInfoSchema } from '../../../../common/form-schemas/modSchemas';
import { updateModField } from '../../../../services/mod.services';
import { useContext } from 'react';
import UserContext from '../../../../context/UserContext';

const StyledButton = InitMainButton({ padding: '10px 10px' });
const StyledTextField = InitMainTextField({ width: '100%' });

const EditModInformation = ({ mod, setMod, opened, handleClose, setDisplayMsg }) => {
  const { user } = useContext(UserContext);

  const formik = useFormik({
    initialValues: {
      modInfo: mod.modInfo.join('\n'),
    },
    validationSchema: object(modInfoSchema),
    onSubmit: async (values) => {
      const newTrimmedModInfo = values.modInfo
        .split('\n')
        .map((str) => str.trim())
        .filter((str) => !!str)
        .join('|n|');

      const currentModInfo = mod.modInfo.join('|n|');

      if (newTrimmedModInfo === currentModInfo)
        return formik.setFieldError('modInfo', 'Mod Info must be different');

      try {
        await updateModField(mod.id, 'modInfo', newTrimmedModInfo);

        await updateModField(mod.id, `lastActivity/${user}`, Number(new Date()));
        setMod({ ...mod, modInfo: newTrimmedModInfo.split('|n|') });
        setDisplayMsg({ opn: true, msg: 'Mod info updated.' });
        handleClose();
      } catch (e) {
        console.error(e);
      }
    },
  });

  return (
    <Modal open={opened} onClose={handleClose}>
      <form onSubmit={formik.handleSubmit}>
        <Box sx={modalStyle}>
          <h2 style={{ paddingBottom: '35px' }}>Edit mod info</h2>
          <StyledTextField
            {...formik.getFieldProps('modInfo')}
            id="modInfo"
            multiline
            rows={10}
            value={formik.values.modInfo}
            error={formik.touched.modInfo && Boolean(formik.errors.modInfo)}
          />
          <ErrorStyled>{formik.errors.modInfo}</ErrorStyled>
          <Box pt="35px">
            <StyledButton type="submit">Save</StyledButton>
          </Box>
        </Box>
      </form>
    </Modal>
  );
};

export default EditModInformation;
