import { Modal } from '@mui/material';
import {
  modalStyle,
  ErrorStyled,
  InitMainButton,
  InitMainTextField,
} from '../../../../common/custom-styles/components';
import { useFormik } from 'formik';
import { Box } from '@mui/system';
import { object } from 'yup';
import { modSpecificationsSchema } from '../../../../common/form-schemas/modSchemas';
import { updateModField } from '../../../../services/mod.services';
import { useContext } from 'react';
import UserContext from '../../../../context/UserContext';

const StyledButton = InitMainButton({ padding: '10px 10px' });
const StyledTextField = InitMainTextField({ width: '100%' });

const EditModSpecificationsModal = ({ mod, setMod, opened, handleClose, setDisplayMsg }) => {
  const { user } = useContext(UserContext);

  const formik = useFormik({
    initialValues: {
      specifications: mod.specifications.join('\n'),
    },
    validationSchema: object(modSpecificationsSchema),
    onSubmit: async (values) => {
      const newTrimmedSpecifications = values.specifications
        .split('\n')
        .map((str) => str.trim())
        .filter((str) => !!str)
        .join('|n|');

      const currentSpecifications = mod.specifications.join('|n|');

      if (newTrimmedSpecifications === currentSpecifications)
        return formik.setFieldError('specifications', 'Specifications must be different');

      try {
        await updateModField(mod.id, 'specifications', newTrimmedSpecifications);

        await updateModField(mod.id, `lastActivity/${user}`, Number(new Date()));
        setMod({ ...mod, specifications: newTrimmedSpecifications.split('|n|') });
        setDisplayMsg({ opn: true, msg: 'Specifications updated.' });
        handleClose();
      } catch (e) {
        console.error(e);
      }
    },
  });

  return (
    <Modal open={opened} onClose={handleClose}>
      <form onSubmit={formik.handleSubmit}>
        <Box sx={modalStyle}>
          <h2 style={{ paddingBottom: '35px' }}>Edit specifications</h2>
          <StyledTextField
            {...formik.getFieldProps('specifications')}
            id="specifications"
            multiline
            rows={10}
            value={formik.values.specifications}
            error={formik.touched.specifications && Boolean(formik.errors.specifications)}
          />
          <ErrorStyled>{formik.errors.specifications}</ErrorStyled>
          <Box pt="35px">
            <StyledButton type="submit">Save</StyledButton>
          </Box>
        </Box>
      </form>
    </Modal>
  );
};

export default EditModSpecificationsModal;
