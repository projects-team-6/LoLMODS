import { Grid, Modal } from '@mui/material';
import {
  modalStyle,
  ErrorStyled,
  InitMainButton,
  InitMainTextField,
} from '../../../../common/custom-styles/components';
import { useFormik } from 'formik';
import { Box } from '@mui/system';
import { object } from 'yup';
import { modYoutubeUrlSchema } from '../../../../common/form-schemas/modSchemas';
import {
  addModImage,
  addModImageBinaryDataToStorage,
  deleteSomeImagesByModId,
  updateModField,
} from '../../../../services/mod.services';
import { useContext, useState } from 'react';

import UserContext from '../../../../context/UserContext';
import Loading from '../../../../components/Loading/Loading';
import FormFieldImages from '../../../CreateMod/FormFieldImages/FormFieldImages';

const StyledButton = InitMainButton({ padding: '10px 10px' });
const StyledTextField = InitMainTextField({ width: '90%' });

const EditPreviewModal = ({ mod, setMod, opened, handleClose, setDisplayMsg }) => {
  const [submitting, setSubmitting] = useState(false);
  const { user } = useContext(UserContext);

  const newImages = mod.images.map((image) => ({
    name: image.split('images%2F')[1].split('?')[0],
  }));

  const formik = useFormik({
    initialValues: {
      images: newImages,
      youtubeURL: mod.youtubeURL_RAW,
    },
    validationSchema: object(modYoutubeUrlSchema),
    onSubmit: async (values) => {
      const trimmedYouTubeURL = values.youtubeURL.trim();

      const changesInImages = !(
        values.images.every((image) =>
          newImages.some((newImage) => newImage.name === image.name)
        ) && values.images.length === mod.images.length
      );

      const changesInYouTubeURL = trimmedYouTubeURL !== mod.youtubeURL_RAW;

      if (!changesInImages && !changesInYouTubeURL) {
        setDisplayMsg({ opn: true, msg: 'Data must be different.' });
        return;
      }

      if (changesInImages) {
        setSubmitting(true);
        setDisplayMsg({ opn: true, msg: 'Images are uploading' });

        try {
          const imageNamesArray = values.images
            .filter((image) => !(image instanceof File))
            .map((image) => image.name);

          const deletedImages = await deleteSomeImagesByModId(mod, imageNamesArray);

          const imageURLs = [];

          for (const image of values.images.filter((image) => image instanceof File)) {
            imageURLs.push(await addModImageBinaryDataToStorage(image, mod.id, 'images'));
          }

          await updateModField(mod.id, `images`, null);

          const allImageUrls = [
            ...mod.images.filter(
              (imageUrl) =>
                !deletedImages.some((deletedImage) => {
                  return imageUrl.includes(encodeURIComponent(deletedImage));
                })
            ),
            ...imageURLs,
          ];

          for (const imageUrl of allImageUrls) {
            await addModImage(mod.id, imageUrl);
          }

          setMod({
            ...mod,
            images: allImageUrls,
          });
          await updateModField(mod.id, `lastActivity/${user}`, Number(new Date()));
          setSubmitting(false);
        } catch (e) {
          console.error(e);
          setDisplayMsg({ opn: true, msg: 'Error ocurred, please try again later.' });
          return;
        }
      }

      if (changesInYouTubeURL) {
        try {
          await updateModField(mod.id, 'youtubeURL', trimmedYouTubeURL);
          await updateModField(mod.id, `lastActivity/${user}`, Number(new Date()));
          setMod({
            ...mod,
            youtubeURL: trimmedYouTubeURL
              ? trimmedYouTubeURL
                  .split('/')
                  [trimmedYouTubeURL.split('/').length - 1].replace('v=', '')
              : null,
          });
        } catch (e) {
          console.error(e);
          setDisplayMsg({ opn: true, msg: 'Error ocurred, please try again later.' });
          return;
        }
      }

      setDisplayMsg({ opn: true, msg: 'Preview updated.' });
      handleClose();
    },
  });

  return (
    <>
      {submitting && <Loading infinite />}
      <Modal open={opened} onClose={handleClose}>
        <form onSubmit={formik.handleSubmit}>
          <Box sx={modalStyle}>
            <h2 style={{ paddingBottom: '35px' }}>Edit main image</h2>

            <Grid container spacing={1}>
              <FormFieldImages formik={formik} StyledButton={StyledButton} height="120px" />
            </Grid>

            {/* {YOUTUBE SECTION} */}
            <Grid container alignContent="center" direction="column">
              <h3>YouTube URL</h3>
              <StyledTextField id="youtube-url" {...formik.getFieldProps('youtubeURL')} />
              <ErrorStyled>{formik.touched.youtubeURL ? formik.errors.youtubeURL : ''}</ErrorStyled>
            </Grid>

            <Box sx={{ marginTop: '35px' }}>
              <StyledButton type="submit">Save</StyledButton>
            </Box>
          </Box>
        </form>
      </Modal>
    </>
  );
};

export default EditPreviewModal;
