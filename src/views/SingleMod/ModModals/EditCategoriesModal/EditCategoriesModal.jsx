import { Modal } from '@mui/material';
import { modalStyle, InitMainButton } from '../../../../common/custom-styles/components';
import { useFormik } from 'formik';
import { Box } from '@mui/system';
import { object } from 'yup';
import { modCategoriesSchema } from '../../../../common/form-schemas/modSchemas';
import { updateModField } from '../../../../services/mod.services';
import { useContext } from 'react';
import UserContext from '../../../../context/UserContext';
import Categories from '../../../../components/Categories/Categories';

const StyledButton = InitMainButton({ padding: '10px 10px' });

const EditCategoriesModal = ({ mod, setMod, opened, handleClose, setDisplayMsg }) => {
  const { user } = useContext(UserContext);

  const formik = useFormik({
    initialValues: {
      mainCategory: mod.mainCategory,
      subCategory: { val: mod.subCategory || '', disabled: mod.subCategory ? false : true },
      subSubCategory: {
        val: mod.subSubCategory || '',
        disabled: mod.subSubCategory ? false : true,
      },
    },
    validationSchema: object(modCategoriesSchema),
    onSubmit: async (values) => {
      if (
        (!values.subCategory.disabled && values.subCategory.val === '') ||
        (!values.subSubCategory.disabled && values.subSubCategory.val === '')
      ) {
        formik.setFieldError('mainCategory', 'Categories are required');
        return;
      }

      if (
        values.mainCategory === mod.mainCategory &&
        values.subCategory.val === mod.subCategory &&
        values.subSubCategory.val === mod.subSubCategory
      ) {
        formik.setFieldError('mainCategory', 'Categories must be different');
        return;
      }

      try {
        if (values.mainCategory !== mod.mainCategory)
          await updateModField(mod.id, 'mainCategory', values.mainCategory);
        if (values.subCategory.val !== mod.subCategory)
          await updateModField(mod.id, 'subCategory', values.subCategory.val);
        if (values.subSubCategory.val !== mod.subSubCategory)
          await updateModField(mod.id, 'subSubCategory', values.subSubCategory.val);

        await updateModField(mod.id, `lastActivity/${user}`, Number(new Date()));
        setMod({
          ...mod,
          mainCategory: values.mainCategory,
          subCategory: values.subCategory.val,
          subSubCategory: values.subSubCategory.val,
        });
        setDisplayMsg({ opn: true, msg: 'Categories updated.' });
        handleClose();
      } catch (e) {
        console.error(e);
      }
    },
  });

  return (
    <Modal open={opened} onClose={handleClose}>
      <form onSubmit={formik.handleSubmit}>
        <Box sx={modalStyle}>
          <h2 style={{ paddingBottom: '35px' }}>Edit categories</h2>
          <Categories formik={formik} />
          <Box pt="35px">
            <StyledButton type="submit">Save</StyledButton>
          </Box>
        </Box>
      </form>
    </Modal>
  );
};

export default EditCategoriesModal;
