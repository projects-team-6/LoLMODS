import { Grid } from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { InitMainButton, lineStyle } from '../../common/custom-styles/components';
import UserContext from '../../context/UserContext';
import { getModById } from '../../services/mod.services';
import { getAvatar } from '../../services/user.services';
import './SingleMod.css';
import ModVersionControl from './ModVersionControl/ModVersionControl';
import ModDetailsField from './ModFields/ModDetailsField/ModDetailsField';
import ModInformationField from './ModFields/ModInformationField/ModInformationField';
import ModSpecificationsField from './ModFields/ModSpecificationsField/ModSpecificationsField';
import ModPreviewField from './ModFields/ModPreviewField/ModPreviewField';
import ModInstallationField from './ModFields/ModInstallationField/ModInstallationField';
import ModUpperButtonsField from './ModFields/ModUpperButtonsField/ModUpperButtonsField';
import ModEndButtonsAndCommentsField from './ModFields/ModEndButtonsAndCommentsField/ModEndButtonsAndCommentsField';

const DownloadButton = InitMainButton({ width: '500px', padding: '20px 0' });
const StyledButton = InitMainButton({ width: '210px' });

const SingleMod = ({ setDisplayMsg }) => {
  const { userData } = useContext(UserContext);
  const [currentMod, setCurrentMod] = useState({});
  const [modals, setModals] = useState({
    editTitle: false,
    editMainImage: false,
    editCategories: false,
    editModInfo: false,
    editSpecifications: false,
    editPreview: false,
    notApproved: false,
  });
  const { modId } = useParams();
  const [modCategories, setModCategories] = useState([]);

  useEffect(() => window.scrollTo(0, 0), []);

  useEffect(() => {
    getModById(modId)
      .then((mods) => {
        const data = mods;
        getAvatar(data.author.username).then((avatar) => {
          setModCategories([data.mainCategory, data.subCategory, data.subSubCategory]);
          return setCurrentMod({ ...data, author: { ...data.author, avatarUrl: avatar } });
        });
      })
      .catch(console.log);
  }, [modId, userData]);

  return (
    <>
      {currentMod.title && (
        <Grid container rowSpacing={3} columnGap={4}>
          <ModUpperButtonsField
            {...{
              modId,
              currentMod,
              setCurrentMod,
              modals,
              setModals,
              setDisplayMsg,
              StyledButton,
            }}
          />

          <Grid item xs="auto">
            <h1>{currentMod?.title}</h1>
          </Grid>

          <Grid item style={lineStyle} xs></Grid>

          <Grid item xs={12}>
            <img
              className="main-img"
              src={currentMod?.mainImage}
              alt="img"
              style={{ border: '10px solid #FD0017', borderRadius: '8px' }}
            />

            <ModDetailsField {...{ currentMod, modCategories, setDisplayMsg, modId }} />
          </Grid>

          <ModInformationField
            {...{
              modId,
              currentMod,
              setCurrentMod,
              modals,
              setModals,
              StyledButton,
              setDisplayMsg,
            }}
          />

          <ModSpecificationsField
            {...{
              modId,
              currentMod,
              setCurrentMod,
              modals,
              setModals,
              StyledButton,
              setDisplayMsg,
            }}
          />

          <ModPreviewField
            {...{
              modId,
              currentMod,
              setCurrentMod,
              modals,
              setModals,
              StyledButton,
              setDisplayMsg,
            }}
          />

          <ModVersionControl
            {...{
              modId,
              currentMod,
              setCurrentMod,
              DownloadButton,
              StyledButton,
              setDisplayMsg,
            }}
          />

          <ModInstallationField {...{ DownloadButton }} />

          <ModEndButtonsAndCommentsField
            {...{
              modId,
              currentMod,
              setCurrentMod,
              modals,
              setModals,
              StyledButton,
              setDisplayMsg,
            }}
          />
        </Grid>
      )}
    </>
  );
};

export default SingleMod;
