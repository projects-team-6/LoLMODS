import { FormControl, FormControlLabel, Grid, Modal, RadioGroup } from '@mui/material';
import {
  modalStyle,
  ErrorStyled,
  InitMainButton,
  InitMainTextField,
  InitRadio,
  iconsStyle,
} from '../../../../common/custom-styles/components';
import { useFormik } from 'formik';
import { Box } from '@mui/system';
import { updateModSchema } from '../../../../common/form-schemas/modSchemas';
import {
  addModBinaryDataToStorage,
  deleteModFileById,
  updateModField,
} from '../../../../services/mod.services';
import { useContext, useState } from 'react';
import UserContext from '../../../../context/UserContext';
import SendIcon from '@mui/icons-material/Send';
import FormFieldMod from '../../../CreateMod/FormFieldMod/FormFieldMod';
import Loading from '../../../../components/Loading/Loading';
import { addMessage } from '../../../../services/message.service';

const StyledButton = InitMainButton({ padding: '10px 10px' });
const UploadButton = InitMainButton({ padding: '15px 10px', width: '60%' });
const StyledTextField = InitMainTextField({ width: '90%' });
const StyledRadio = InitRadio();

const UpdateModModal = ({ mod, setMod, opened, handleClose, setDisplayMsg }) => {
  const { user } = useContext(UserContext);
  const [submitting, setSubmitting] = useState(false);

  const formik = useFormik({
    initialValues: {
      mod: null,
      versionMessage: '',
      versionType: '',
    },
    validationSchema: updateModSchema,
    onSubmit: async (values) => {
      const trimmedVersionMessage = values.versionMessage.trim();

      if (mod.binaryContent.includes(values.mod.name))
        Object.defineProperty(values.mod, 'name', {
          writable: true,
          value: `new-${values.mod.name}`,
        });

      if (mod.newVersion) {
        try {
          await deleteModFileById(mod.id, mod.binaryContent.split('token=')[1]);
          await updateModField(mod.id, 'newVersion', null);
        } catch (e) {
          console.error(e);
        }
      }

      setSubmitting(true);
      setDisplayMsg({ opn: true, msg: 'Mod is uploading' });

      try {
        const downloadUrl = await addModBinaryDataToStorage(values.mod, mod.id);

        const newVersion = {
          versionMessage: trimmedVersionMessage,
          versionType: values.versionType,
          mod: downloadUrl,
        };

        await updateModField(mod.id, `newVersion`, newVersion);

        await updateModField(mod.id, `lastActivity/${user}`, Number(new Date()));
        setMod({ ...mod, newVersion });
        setDisplayMsg({ opn: true, msg: 'Mod updated.' });
        addMessage(
          `"${mod.title}" version update is waiting for approval`,
          user,
          mod.id,
          null,
          true
        );
        handleClose();
      } catch (e) {
        setDisplayMsg({ opn: true, msg: 'Error ocurred, please try again later.' });
        console.error(e);
      }
    },
  });

  return (
    <>
      {submitting && <Loading infinite />}
      <Modal open={opened} onClose={handleClose}>
        <form onSubmit={formik.handleSubmit}>
          <Box sx={modalStyle}>
            <h2 style={{ paddingBottom: '35px' }}>Update mod</h2>

            <FormFieldMod formik={formik} StyledButton={StyledButton} />

            <Grid container alignContent="center" direction="column">
              <h3>Version message</h3>
              <StyledTextField
                {...formik.getFieldProps('versionMessage')}
                id="version-message"
                value={formik.values.versionMessage}
                error={formik.touched.versionMessage && Boolean(formik.errors.versionMessage)}
              />
              <ErrorStyled>
                {formik.touched.versionMessage ? formik.errors.versionMessage : ''}
              </ErrorStyled>
            </Grid>

            <Box pt="35px">
              <FormControl>
                <h3 id="version-type">Version type</h3>
                <RadioGroup
                  aria-labelledby="version-type"
                  row
                  value={formik.values.versionType}
                  onChange={(e) => formik.setFieldValue('versionType', e.target.value)}
                >
                  <FormControlLabel value="major" control={<StyledRadio />} label="Major" />
                  <FormControlLabel value="minor" control={<StyledRadio />} label="Minor" />
                </RadioGroup>
              </FormControl>
              <ErrorStyled style={{ textAlign: 'center' }}>
                {formik.touched.versionType ? formik.errors.versionType : ''}
              </ErrorStyled>
            </Box>

            <Box pt="40px">
              <UploadButton type="submit">
                <SendIcon sx={iconsStyle} />
                Upload for approval
              </UploadButton>
            </Box>
          </Box>
        </form>
      </Modal>
    </>
  );
};

export default UpdateModModal;
