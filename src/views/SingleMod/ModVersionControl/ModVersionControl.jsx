import { Chip, Grid, Stack, Typography } from '@mui/material';
import { useContext, useState } from 'react';
import { iconsStyle, lineStyle } from '../../../common/custom-styles/components';
import UserContext from '../../../context/UserContext';
import { addMessage } from '../../../services/message.service';
import { deleteModFileById, updateModField } from '../../../services/mod.services';
import NotApprovedModal from '../ModModals/NotApprovedModal/NotApprovedModal';
import UpdateModModal from './UpdateModModal/UpdateModModal';
import UpdateIcon from '@mui/icons-material/Update';

const ApprovingButtonsStyle = { marginLeft: '20px', width: '130px', padding: '6px 0' };

export default function ModVersionControl({
  modId,
  currentMod,
  setCurrentMod,
  DownloadButton,
  StyledButton,
  setDisplayMsg,
}) {
  const { userData } = useContext(UserContext);
  const [modals, setModals] = useState({
    notApprovedModal: false,
    updateModModal: false,
  });

  const versions =
    currentMod?.versions &&
    currentMod.versions.sort(([a, _], [b, __]) => {
      const [firstA, secondA] = a.split('-');
      const [firstB, secondB] = b.split('-');
      if (firstA !== firstB) {
        return +firstB - +firstA;
      } else {
        return +secondB - +secondA;
      }
    });

  const handleIncrementDownloads = async () => {
    try {
      await updateModField(modId, 'downloads', currentMod?.downloads + 1);
      setCurrentMod({ ...currentMod, downloads: currentMod?.downloads + 1 });
    } catch (e) {
      console.error(e);
    }
  };

  const handleApprove = async (newVersion) => {
    const newVersionURL = currentMod.newVersion.mod;
    const newVersionMessage = currentMod.newVersion.versionMessage;
    const newVersionDashed = newVersion.split('.').join('-');

    try {
      await deleteModFileById(currentMod.id, currentMod.binaryContent.split('token=')[1]);
      await updateModField(currentMod.id, 'binaryContent', newVersionURL);
      await updateModField(currentMod.id, `versions/${newVersionDashed}`, newVersionMessage);
      await updateModField(currentMod.id, 'newVersion', null);

      setCurrentMod({
        ...currentMod,
        versions: [...currentMod.versions, [newVersionDashed, newVersionMessage]],
        binaryContent: newVersionURL,
        newVersion: null,
      });

      setDisplayMsg({ opn: true, msg: 'New version approved.' });
      addMessage(
        `"${currentMod.title}" version update was approved`,
        userData.username,
        currentMod.id,
        currentMod.author.username
      );
    } catch (e) {
      setDisplayMsg({ opn: true, msg: 'Error ocurred, please try again later.' });
      console.error(e);
    }
  };

  let newVersion = currentMod.newVersion && currentMod.versions[0][0];

  if (newVersion) {
    if (currentMod.newVersion.versionType === 'minor')
      newVersion = `${newVersion.split('-')[0]}.${+newVersion.split('-')[1] + 1}`;
    else if (currentMod.newVersion.versionType === 'major')
      newVersion = `${+newVersion.split('-')[0] + 1}.0`;
  }

  return (
    <>
      {/* Download link */}
      <Grid item style={lineStyle} xs={12}></Grid>
      <Grid item xs={12}>
        <h2>DOWNLOAD</h2>
      </Grid>
      <Grid item xs={12} textAlign="center">
        <a href={currentMod?.binaryContent} download onClick={handleIncrementDownloads}>
          <DownloadButton className="install-button" variant="outlined">
            {`Download ${currentMod?.title}`}
          </DownloadButton>
        </a>
      </Grid>

      {/* Change log */}
      <Grid item style={lineStyle} xs={12}></Grid>
      <Grid item xs={12}>
        <Stack direction="row" justifyContent="space-between" alignItems="center">
          <h2>CHANGE LOG</h2>
          {userData?.uid === currentMod?.author?.uid || userData?.mods[currentMod.id] ? (
            <StyledButton
              onClick={() => setModals(() => setModals({ ...modals, updateModModal: true }))}
            >
              <UpdateIcon sx={iconsStyle} />
              Update mod
            </StyledButton>
          ) : null}
        </Stack>

        {modals.updateModModal && (
          <UpdateModModal
            mod={currentMod}
            setMod={setCurrentMod}
            opened={modals.updateModModal}
            handleClose={() => setModals(() => setModals({ ...modals, updateModModal: false }))}
            setDisplayMsg={setDisplayMsg}
          />
        )}
      </Grid>
      <Grid item xs={12}>
        <Stack direction="column" rowGap={3} marginLeft="10px" alignItems="start">
          {(userData?.admin ||
            userData?.uid === currentMod?.author?.uid ||
            userData?.mods[currentMod?.id]) &&
          currentMod.newVersion ? (
            <>
              <Typography
                component="span"
                variant="button"
                sx={{ color: '#999999', display: 'inline-block' }}
              >
                <Chip
                  label={newVersion}
                  sx={{
                    backgroundColor: '#4e4e4e',
                    fontWeight: 'bold',
                    fontSize: '14px',
                    letterSpacing: '1px',
                    marginRight: '10px',
                    '& span': {
                      color: '#999999',
                    },
                  }}
                />
                {currentMod.newVersion.versionMessage}

                {userData.admin && (
                  <>
                    <StyledButton
                      style={ApprovingButtonsStyle}
                      onClick={() => handleApprove(newVersion)}
                    >
                      Approve
                    </StyledButton>

                    <StyledButton
                      style={ApprovingButtonsStyle}
                      onClick={() =>
                        setModals(() => setModals({ ...modals, notApprovedModal: true }))
                      }
                    >
                      Not Approve
                    </StyledButton>
                  </>
                )}

                <a href={currentMod?.newVersion?.mod} download onClick={handleIncrementDownloads}>
                  <StyledButton
                    style={ApprovingButtonsStyle}
                    className="install-button"
                    variant="outlined"
                  >
                    Download
                  </StyledButton>
                </a>

                {modals.notApprovedModal && (
                  <NotApprovedModal
                    mod={currentMod}
                    setMod={setCurrentMod}
                    opened={modals.notApprovedModal}
                    handleClose={() =>
                      setModals(() => setModals({ ...modals, notApprovedModal: false }))
                    }
                    setDisplayMsg={setDisplayMsg}
                    type="update"
                  />
                )}
                <div style={{ borderBottom: '1px dotted red', marginTop: '25px' }} />
              </Typography>
            </>
          ) : null}
          {versions &&
            versions.map(([version, versionMessage]) => (
              <Typography component="span" variant="button" key={version}>
                <Chip
                  label={version.split('-').join('.')}
                  sx={{
                    backgroundColor: '#4e4e4e',
                    fontWeight: 'bold',
                    fontSize: '14px',
                    letterSpacing: '1px',
                    marginRight: '10px',
                  }}
                />
                {versionMessage}
              </Typography>
            ))}
        </Stack>
      </Grid>
    </>
  );
}
