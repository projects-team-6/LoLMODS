import { useFormik } from 'formik';
import { Grid, Tabs, Tab, ThemeProvider } from '@mui/material';
import './Register.css';
import { NavLink, useNavigate } from 'react-router-dom';
import { registerUser } from '../../services/auth.service';
import { createUserUsername, getUserByUsername } from '../../services/user.services';
import {
  ErrorStyled,
  InitMainButton,
  InitMainTextField,
} from '../../common/custom-styles/components';
import { registerSchema } from '../../common/form-schemas/userSchemas';
import { loginRegisterTheme } from '../../common/custom-styles/themes';

const StyledTextField = InitMainTextField();
const StyledButton = InitMainButton();

const Register = ({ setDisplayMsg }) => {
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: registerSchema,
    onSubmit: (values) => {
      getUserByUsername(values.userName)
        .then((snapshot) => {
          if (snapshot.exists()) {
            setDisplayMsg({ opn: true, msg: 'Username already exists!' });
            return;
          }

          return registerUser(values.email, values.password)
            .then((u) => {
              createUserUsername(values.userName, u.user.uid, u.user.email, values.phoneNumber)
                .then(() => {
                  setDisplayMsg({ opn: true, msg: `Account is created successfully!` });
                  navigate('/');
                })
                .catch((error) => {
                  console.log(error);
                  setDisplayMsg({ opn: true, msg: `Oops something went wrong!` });
                  return;
                });
            })
            .catch((e) => {
              if (e.message.includes(`email-already-in-use`)) {
                setDisplayMsg({
                  opn: true,
                  msg: `Email ${values.email} has already been registered!`,
                });
                return;
              }
            });
        })
        .catch((error) => {
          console.log(error);
          setDisplayMsg({ opn: true, msg: `Oops something went wrong!` });
        });
    },
  });

  return (
    <ThemeProvider theme={loginRegisterTheme}>
      <div className="auth-container">
        <form onSubmit={formik.handleSubmit}>
          <Grid
            container
            spacing={4}
            direction="column"
            alignItems="center"
            justifyContent="center"
          >
            <Grid item xs={3} className="header-auth">
              <Tabs value={1}>
                <NavLink to={'/login'}>
                  <Tab label="Login" />
                </NavLink>

                <NavLink to={'/register'}>
                  <Tab label="Register" />
                </NavLink>
              </Tabs>
            </Grid>

            <Grid item xs={1}>
              <h4>Username</h4>
              <StyledTextField
                color="primary"
                className="register-input"
                id="userName"
                name="userName"
                // label="Username"
                value={formik.values.userName}
                onChange={formik.handleChange}
                error={formik.touched.userName && Boolean(formik.errors.userName)}
              />
              <ErrorStyled>{formik.touched.userName ? formik.errors.userName : ''}</ErrorStyled>
            </Grid>

            <Grid item xs={1}>
              <h4>Email</h4>
              <StyledTextField
                className="register-input"
                id="email"
                name="email"
                // label="Email"
                value={formik.values.email}
                onChange={formik.handleChange}
                error={formik.touched.email && Boolean(formik.errors.email)}
              />
              <ErrorStyled>{formik.touched.email ? formik.errors.email : ''}</ErrorStyled>
            </Grid>

            <Grid item xs={1}>
              <h4>Phone Number</h4>
              <StyledTextField
                className="register-input"
                id="phoneNumber"
                name="phoneNumber"
                // label="Phone Number"
                type="text"
                value={formik.values.phoneNumber}
                onChange={formik.handleChange}
                error={formik.touched.phoneNumber && Boolean(formik.errors.phoneNumber)}
              />
              <ErrorStyled>
                {formik.touched.phoneNumber ? formik.errors.phoneNumber : ''}
              </ErrorStyled>
            </Grid>

            <Grid item xs={1}>
              <h4>Password</h4>
              <StyledTextField
                className="register-input"
                id="password"
                name="password"
                // label="Password"
                type="password"
                value={formik.values.password}
                onChange={formik.handleChange}
                error={formik.touched.password && Boolean(formik.errors.password)}
              />
              <ErrorStyled>{formik.touched.password ? formik.errors.password : ''}</ErrorStyled>
            </Grid>

            <Grid item xs={1}>
              <h4>Confirm password</h4>
              <StyledTextField
                className="register-input"
                id="repeatPassword"
                name="repeatPassword"
                // label="Confirm Password"
                type="password"
                value={formik.values.repeatPassword}
                onChange={formik.handleChange}
                error={formik.touched.repeatPassword && Boolean(formik.errors.repeatPassword)}
              />
              <ErrorStyled>
                {formik.touched.repeatPassword ? formik.errors.repeatPassword : ''}
              </ErrorStyled>
            </Grid>

            <Grid item xs={1}>
              <StyledButton type="submit">Register</StyledButton>
            </Grid>
            <Grid item xs={1}>
              <NavLink to={'/login'}>Already have an account?</NavLink>
            </Grid>
          </Grid>
        </form>
      </div>
    </ThemeProvider>
  );
};

export default Register;
