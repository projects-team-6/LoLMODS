import {
  Avatar,
  Box,
  Grid,
  ListItem,
  Stack,
  Link,
  IconButton,
  ButtonGroup,
  Button,
} from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import UserContext from '../../context/UserContext';
import FacebookIcon from '@mui/icons-material/Facebook';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import TwitterIcon from '@mui/icons-material/Twitter';
import PublicIcon from '@mui/icons-material/Public';

import './Profile.css';
import { getUserByUsername } from '../../services/user.services';
import { getMaintainModsByUsername, getModsByUsername } from '../../services/mod.services';
import ModCard from '../../components/ModCard/ModCard';
import CustomPagination from '../../components/CustomPagination/CustomPagination';
import { lineStyle } from '../../common/custom-styles/components';

const Profile = ({ view = 'mods' }) => {
  const { user, userData } = useContext(UserContext);
  const [currentUser, setCurrentUser] = useState(null);

  const [mods, setMods] = useState([]);
  const [notApprovedMods, setNotApprovedMods] = useState([]);
  const [maintainMods, setMaintainMods] = useState([]);

  const { userName } = useParams();

  const navigate = useNavigate();

  useEffect(() => {
    if (user === userName) {
      setCurrentUser({ ...userData });
    }

    getUserByUsername(userName)
      .then((snapshot) => {
        const user = snapshot.val();
        setCurrentUser({ ...user });
        return getMaintainModsByUsername(userName).then((mods) => {
          setMaintainMods(mods.reverse());
          if (view === 'maintain' && mods.length === 0) return navigate(`/profile/${userName}`);
        });
      })
      .catch(console.error);
  }, [userName]);

  useEffect(() => {
    (async () => {
      try {
        const modsSnapshot = await getModsByUsername(userName);
        if (modsSnapshot.exists()) {
          setMods(
            Object.entries(modsSnapshot.val())
              .filter(([id, mod]) => mod.approved === true)
              .reverse()
          );
          setNotApprovedMods(
            Object.entries(modsSnapshot.val())
              .filter(([id, mod]) => !mod.approved)
              .reverse()
          );
        } else {
          setMods([]);
        }
      } catch (e) {
        console.error(e);
      }
    })();
  }, [userName]);

  if (view === 'pending' && user !== userName) return navigate(`/profile/${userName}`);

  return (
    <>
      <Box className="user-box">
        <Grid container direction="row" alignItems="flex-start" columnSpacing={{ xs: 2 }}>
          <Avatar
            xs={2}
            alt="avatar"
            src={currentUser?.avatarUrl ? currentUser?.avatarUrl : null}
            sx={{ width: 120, height: 120, fontSize: 60, m: 2, ml: 5 }}
          >
            {!currentUser?.avatarUrl ? currentUser?.username[0].toUpperCase() : null}
          </Avatar>
          <Stack xs={8} sx={{ m: 2 }}>
            <ListItem>
              <h2>{currentUser?.username}</h2>
            </ListItem>
            <ListItem>
              <p>{currentUser?.aboutMe}</p>
            </ListItem>
            <ListItem>
              {currentUser?.facebook ? (
                <Link href={currentUser?.facebook ? currentUser?.facebook : null} target="_blank">
                  <IconButton>
                    <FacebookIcon />
                  </IconButton>
                </Link>
              ) : null}
              {currentUser?.linkedin ? (
                <Link href={currentUser?.linkedin ? currentUser?.linkedin : null} target="_blank">
                  <IconButton>
                    <LinkedInIcon />
                  </IconButton>
                </Link>
              ) : null}
              {currentUser?.twitter ? (
                <Link href={currentUser?.twitter ? currentUser?.twitter : null} target="_blank">
                  <IconButton>
                    <TwitterIcon />
                  </IconButton>
                </Link>
              ) : null}
              {currentUser?.website ? (
                <Link href={currentUser?.website ? currentUser?.website : null} target="_blank">
                  <IconButton>
                    <PublicIcon />
                  </IconButton>
                </Link>
              ) : null}
            </ListItem>
          </Stack>
          {currentUser?.blocked ? (
            <Grid item xs={12} sx={{ m: 2, textAlign: 'right' }}>
              <h2 className="profile-blocked">Blocked</h2>
            </Grid>
          ) : null}
        </Grid>
      </Box>

      <ButtonGroup variant="text" aria-label="text button group" color="inherit">
        <Button
          onClick={() => navigate(`/profile/${userName}`, { replace: true })}
          style={{ borderBottom: view === 'mods' && '2px solid #FD0017' }}
        >
          Mods
        </Button>
        {user === userName && notApprovedMods.length > 0 ? (
          <Button
            onClick={() => navigate(`/profile/${userName}/pending`, { replace: true })}
            style={{ borderBottom: view === 'pending' && '2px solid #FD0017' }}
          >
            Waiting for approval
          </Button>
        ) : null}
        {maintainMods.length > 0 ? (
          <Button
            onClick={() => navigate(`/profile/${userName}/maintain`, { replace: true })}
            style={{ borderBottom: view === 'maintain' && '2px solid #FD0017' }}
          >
            Maintained Mods
          </Button>
        ) : null}
      </ButtonGroup>

      <Box marginTop="40px">
        {view === 'mods' && (
          <>
            <Grid container rowSpacing={4} columnGap={4}>
              <Grid item xs="auto">
                <h1>Mods</h1>
              </Grid>
              <Grid item style={lineStyle} xs></Grid>
              <Grid item container spacing={2} xs={12}>
                <CustomPagination>
                  {mods.map(([id, mod]) => {
                    return <ModCard mod={{ ...mod, id }} key={id} />;
                  })}
                </CustomPagination>
              </Grid>
            </Grid>
          </>
        )}
        {view === 'pending' && (
          <>
            <Grid container rowSpacing={4} columnGap={4}>
              <Grid item xs="auto">
                <h1>Waiting for approval</h1>
              </Grid>
              <Grid item style={lineStyle} xs></Grid>
              <Grid item container spacing={2} xs={12}>
                <CustomPagination>
                  {notApprovedMods.map(([id, mod]) => {
                    return <ModCard mod={{ ...mod, id }} key={id} />;
                  })}
                </CustomPagination>
              </Grid>
            </Grid>
          </>
        )}
        {view === 'maintain' && (
          <>
            <Grid container rowSpacing={4} columnGap={4}>
              <Grid item xs="auto">
                <h1>Maintained mods</h1>
              </Grid>
              <Grid item style={lineStyle} xs></Grid>
              <Grid item container spacing={2} xs={12}>
                <CustomPagination>
                  {maintainMods.map(([id, mod]) => {
                    return <ModCard mod={{ ...mod, id }} key={id} />;
                  })}
                </CustomPagination>
              </Grid>
            </Grid>
          </>
        )}
      </Box>
    </>
  );
};

export default Profile;
