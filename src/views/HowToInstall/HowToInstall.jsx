import { Grid } from '@mui/material';
import { INSTALL_REQUIREMENTS, INSTALL_URL, INSTALL_VIDEO_ID } from '../../common/constants';
import { InitMainButton, lineStyle } from '../../common/custom-styles/components';
import YoutubeEmbed from '../../components/YoutubeEmbed/YoutubeEmbed';
import './HowToInstall.css';

const DownloadButton = InitMainButton({ padding: '25px 0', width: '500px' });

const HowToInstall = () => {
  window.scrollTo(0, 0);

  return (
    <Grid container rowSpacing={3} columnGap={4}>
      <Grid item xs="auto">
        <h1>HOW TO INSTALL MODS</h1>
      </Grid>
      <Grid item style={lineStyle} xs></Grid>
      <Grid item xs={12}>
        <img className="install-img" src={process.env.PUBLIC_URL + '/install.png'} alt="img" />
      </Grid>
      <Grid item style={lineStyle} xs={12}></Grid>
      <Grid item xs={12}>
        <h2>REQUIREMENTS TO RUN LCS</h2>
      </Grid>
      <Grid item xs={12}>
        <ul className="info-and-specs">
          {INSTALL_REQUIREMENTS.map((r, i) => (
            <li key={i}>{r}</li>
          ))}
        </ul>
      </Grid>
      <Grid item style={lineStyle} xs={12}></Grid>
      <Grid item xs={12}>
        <h2>DOWNLOAD</h2>
      </Grid>
      <Grid item xs={12} textAlign="center">
        <a href={INSTALL_URL}>
          <DownloadButton className="install-button" variant="outlined">
            Download newest release from GitHub
          </DownloadButton>
        </a>
      </Grid>
      <Grid item style={lineStyle} xs={12}></Grid>
      <Grid item xs={12}>
        <h2>VIDEO TUTORIAL</h2>
      </Grid>
      <Grid item xs={12} textAlign="center">
        <YoutubeEmbed embedId={INSTALL_VIDEO_ID} />
      </Grid>
    </Grid>
  );
};

export default HowToInstall;
