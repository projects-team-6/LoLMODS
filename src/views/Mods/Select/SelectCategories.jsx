import { ThemeProvider } from '@mui/material';
import FormControl from '@mui/material/FormControl';
import { InitMainMenuItem, InitMainSelect } from '../../../common/custom-styles/components';
import { categoriesTheme } from '../../../common/custom-styles/themes';

const StyledSelect = InitMainSelect();
const StyledMenuItem = InitMainMenuItem();

export default function SelectSCategories({
  
  type,
  params,
  setParams,
  setSearchParams,
  categories,
  cat = 'All',
  sort = false,
}) {
  const handleChange = (event) => {

    let newParams;
    if (type==='mainCategory') {
      newParams = { ...params, mainCategory: event.target.value, subCategory: 'All', subSubCategory: 'All' }   
    } else if (type === 'subCategory') {
      newParams = { ...params, subCategory: event.target.value, subSubCategory: 'All' }
    } else {
      newParams = { ...params, [type]: event.target.value }
    }

    setSearchParams({
        searchParam: newParams.search,
        mainCat: newParams.mainCategory,
        subCat: newParams.subCategory,
        subSubCat: newParams.subSubCategory,
        sort: newParams.sort
      }) 
  };

  return (
    <ThemeProvider theme={categoriesTheme}>
      <FormControl sx={{ m: 1, minWidth: 120 }} size="small">
        <StyledSelect id="cat-select" value={cat} onChange={handleChange}>
          {!sort ? <StyledMenuItem value="All">All</StyledMenuItem> : null}

          {categories.map((category) => {
            return (
              <StyledMenuItem key={category} value={category}>
                {category}
              </StyledMenuItem>
            );
          })}
        </StyledSelect>
      </FormControl>
    </ThemeProvider>
  );
}
