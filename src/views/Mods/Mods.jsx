import { Grid, IconButton, InputAdornment, Stack } from '@mui/material';
import { InitMainTextField, lineStyle } from '../../common/custom-styles/components';
import SearchIcon from '@mui/icons-material/Search';
import { categories } from '../../common/categories';
import { Box } from '@mui/system';
import CustomPagination from '../../components/CustomPagination/CustomPagination';
import ModCard from '../../components/ModCard/ModCard';
import { useState } from 'react';
import { useEffect } from 'react';
import { getApprovedMods } from '../../services/mod.services';
import SelectSCategories from './Select/SelectCategories';
import { createSearchParams, useNavigate, useSearchParams } from 'react-router-dom';

const StyledTextField = InitMainTextField({ p: '2px 4px', display: 'flex', width: 400 });

const Mods = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const [mods, setMods] = useState([]);
  const [params, setParams] = useState({});

  const navigate = useNavigate();

  useEffect(() => {
    setParams({
      search: searchParams.get('searchParam').trim().toLowerCase() || '',
      mainCategory: searchParams.get('mainCat') || 'All',
      subCategory: searchParams.get('subCat') || 'All',
      subSubCategory: searchParams.get('subSubCat') || 'All',
      sort: searchParams.get('sort') || 'New',
    })
  },[searchParams])
  useEffect(() => {
    getApprovedMods().then((snapshot) => {
      if (!snapshot.exists()) return [];

      let result = Object.entries(snapshot.val());
      if (params?.search !== '')
        result = result.filter(([id, mod]) => (mod?.title).toLowerCase().startsWith(params?.search));

      if (params?.subSubCategory !== '' && params?.subSubCategory !== 'All') {
        result = result.filter(([id, mod]) => mod.subSubCategory === params?.subSubCategory);
      } else if (params?.subCategory !== '' && params?.subCategory !== 'All') {
        result = result.filter(([id, mod]) => mod.subCategory === params?.subCategory);
      } else if (params?.mainCategory !== 'All') {
        result = result.filter(([id, mod]) => mod.mainCategory === params?.mainCategory);
      }

      switch (params?.sort) {
        case 'New':
          result = result.sort(([idA, a], [idB, b]) => {
            return b.createdOn - a.createdOn;
          });
          break;
        case 'Title':
          result = result.sort(([idA, a], [idB, b]) =>
            a.title.toLocaleLowerCase() > b.title.toLocaleLowerCase()
              ? 1
              : b.title.toLocaleLowerCase() > a.title.toLocaleLowerCase()
              ? -1
              : 0
          );
          break;
        case 'Downloads':
          result = result.sort(([idA, a], [idB, b]) => {
            return (b.downloads ? b.downloads : 0) - (a.downloads ? a.downloads : 0);
          });
          break;
        case 'Rating':
          result = result.sort(([idA, a], [idB, b]) => {
            const ratingB = b.rating
              ? Object.values(b.rating).reduce((acc, rating) => acc + rating, 0) /
                Object.keys(b.rating).length
              : 0;
            const ratingA = a.rating
              ? Object.values(a.rating).reduce((acc, rating) => acc + rating, 0) /
                Object.keys(a.rating).length
              : 0;
            return ratingB - ratingA;
          });
          break;
        default:
          break;
      }

      
      setMods([...result]);
    });
  }, [params]);


  const handelSearch = (searchString) => {
    navigate(
      {
        pathname: '/mods',
        search: createSearchParams({
          searchParam: searchString,
          mainCat: params.mainCategory,
          subCat: params.subCategory,
          subSubCat: params.subSubCategory,
          sort: params.sort,
        }).toString(),
      },
      { replace: true }
    );

  };

  const handelEnter = (e) => {
    e.preventDefault();
    if (e.key === 'Enter') {
      handelSearch(e.target.value);
    }
  };

  return (
    <Grid container direction="row" rowSpacing={4} columnGap={4}>
      <Grid item xs="auto">
        <h1>Mods</h1>
      </Grid>
      <Grid item style={lineStyle} xs></Grid>
      <Grid item xs={12} alignSelf="center">
        <form
          onSubmit={(e) => {
            e.preventDefault();
            handelSearch(e.target[1].value);
          }}
        >
          <StyledTextField
            onKeyUp={handelEnter}
            autoComplete="off"
            size="small"
            type="search"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <IconButton type="submit">
                    <SearchIcon sx={{ fill: '#181C23' }} />
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </form>
      </Grid>
      <Grid item xs={12}>
        <Stack direction="row">
          <Stack sx={{ width: 500 }}>{`${mods.length} ${
            mods.length === 1 ? 'Result' : 'Results'
          }`}</Stack>
          <Stack direction="column">
            Categories
            <SelectSCategories
              categories={Object.keys(categories).map((cat) => {
                if (typeof cat === 'object') return Object.keys(cat)[0];
                return cat;
              })}
              type={'mainCategory'}
              setSearchParams={setSearchParams}
              params={params}
              setParams={setParams}
              cat={params.mainCategory}
            />
          </Stack>
          {categories[params.mainCategory] !== null && categories[params.mainCategory] !== undefined ? (
            <Stack direction="column">
              Sub Categories
              <SelectSCategories
                categories={Object.keys(categories[params.mainCategory]).map((cat) => {
                  if (typeof cat === 'object') return Object.keys(cat)[0];
                  return cat;
                })}
                setSearchParams={setSearchParams}
                type={'subCategory'}
                params={params}
                setParams={setParams}
                cat={params.subCategory}
              />
            </Stack>
          ) : null}
          {categories[params.mainCategory] !== null && categories[params.mainCategory] !== undefined ? (
            categories[params.mainCategory][params.subCategory] !== null &&
            categories[params.mainCategory][params.subCategory] !== undefined ? (
              <Stack direction="column">
                Sub Sub Categories
                <SelectSCategories
                  categories={Object.keys(categories[params.mainCategory][params.subCategory]).map((cat) => {
                    if (typeof cat === 'object') return Object.keys(cat)[0];
                    return cat;
                  })}
                  setSearchParams={setSearchParams}
                  type={'subSubCategory'}
                  cat={params.subSubCategory}
                  setParams={setParams}
                  params={params}
                />
              </Stack>
            ) : null
          ) : null}
          <Stack sx={{ flexGrow: 1 }}></Stack>
          <Stack direction="column">
            Sort
            <SelectSCategories
              categories={['New', 'Rating', 'Downloads', 'Title'].map((cat) => {
                return cat;
              })}
              setSearchParams={setSearchParams}
              type={'sort'}
              params={params}
              setParams={setParams}
              cat={params.sort || ''}
              sort={true}
            />
          </Stack>
        </Stack>
      </Grid>
      <Box marginTop="40px" width="100%">
        <Grid container spacing={2}>
          <CustomPagination>
            {mods.map(([id, mod]) => {
              return <ModCard mod={{ ...mod, id }} key={id} />;
            })}
          </CustomPagination>
        </Grid>
      </Box>
    </Grid>
  );
};

export default Mods;
