import { Grid } from '@mui/material';
import { resetPassword } from '../../services/auth.service';
import { useFormik } from 'formik';
import { useNavigate } from 'react-router-dom';
import {
  ErrorStyled,
  InitMainButton,
  InitMainTextField,
} from '../../common/custom-styles/components';
import { resetPasswordSchema } from '../../common/form-schemas/userSchemas';

const StyledTextField = InitMainTextField();
const StyledButton = InitMainButton();

const ResetPassword = ({ setDisplayMsg }) => {
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      email: '',
    },
    validationSchema: resetPasswordSchema,
    onSubmit: (values) => {
      resetPassword(values.email)
        .then(() => {
          navigate('/login');
          setDisplayMsg({ opn: true, msg: 'Link send successfully' });
        })
        .catch(console.error);
    },
  });

  return (
    <div className="auth-container">
      <form onSubmit={formik.handleSubmit}>
        <Grid
          container
          spacing={4}
          direction="column"
          alignItems="center"
          justifyContent="center"
          style={{ minHeight: '30vh' }}
        >
          <Grid item>
            <h4>Email</h4>
            <StyledTextField
              className="login-input"
              id="email"
              name="email"
              // label="Email"
              value={formik.values.email}
              onChange={formik.handleChange}
              error={formik.touched.email && Boolean(formik.errors.email)}
            />
            <ErrorStyled>{formik.touched.email ? formik.errors.email : ''}</ErrorStyled>
          </Grid>
          <Grid item>
            <StyledButton type="submit">Send</StyledButton>
          </Grid>
        </Grid>
      </form>
    </div>
  );
};

export default ResetPassword;
