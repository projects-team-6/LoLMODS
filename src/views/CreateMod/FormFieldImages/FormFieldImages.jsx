import { Chip, Grid } from '@mui/material';
import { uploadImagesIcons } from '../../../common/custom-styles/components';

export default function FormFieldImages({ formik, StyledButton, height = '60px' }) {
  return (
    <>
      <Grid item xs={12} container justifyContent="center">
        <label htmlFor="images">
          <input
            type="file"
            id="images"
            accept="image/*"
            value=""
            disabled={formik.values.images.length === 5}
            onChange={(e) => {
              const uploadedImage = e.target.files[0];
              if (
                uploadedImage &&
                formik.values.images.length < 5 &&
                !formik.values.images.some((image) => image?.name === uploadedImage.name)
              ) {
                formik.setFieldValue('images', [...formik.values.images, uploadedImage]);
              }
            }}
          />
          <StyledButton component="span">
            {uploadImagesIcons[formik.values.images.length]}
            Images
          </StyledButton>
        </label>
      </Grid>

      <Grid item xs={12} container spacing={1} justifyContent="center" sx={{ height }}>
        {formik.values.images &&
          formik.values.images.map((image) => (
            <Grid item key={image.name}>
              <Chip
                label={image.name.length > 20 ? `${image.name.slice(0, 16)}...` : image.name}
                variant="outlined"
                onDelete={() => {
                  const filteredImages = formik.values.images.filter(
                    (mapImage) => mapImage.name !== image.name
                  );
                  formik.setFieldValue('images', filteredImages);
                }}
              />
            </Grid>
          ))}
      </Grid>
    </>
  );
}
