import './CreateMod.scss';
import { useFormik } from 'formik';
import { useContext, useState } from 'react';
import { MESSAGE_FOR_APPROVE } from '../../common/constants';
import UserContext from '../../context/UserContext';
import {
  addModBinaryDataToStorage,
  addModImage,
  addModImageBinaryDataToStorage,
  createMod,
  updateModField,
} from '../../services/mod.services';
import { addMessage } from '../../services/message.service';
import { updateUserField } from '../../services/user.services';
import Categories from '../../components/Categories/Categories';
import { Grid } from '@mui/material';
import {
  ErrorStyled,
  iconsStyle,
  InitMainButton,
  InitMainTextField,
} from '../../common/custom-styles/components';
import SendIcon from '@mui/icons-material/Send';
import { useNavigate } from 'react-router-dom';
import { createModSchema } from '../../common/form-schemas/modSchemas';
import Loading from './../../components/Loading/Loading';
import FormFieldMod from './FormFieldMod/FormFieldMod';
import FormFieldMainImage from './FormFieldMainImage/FormFieldMainImage';
import FormFieldImages from './FormFieldImages/FormFieldImages';

const StyledTextField = InitMainTextField({ width: '80%' });
const StyledButton = InitMainButton();

export default function CreateMod({ setDisplayMsg }) {
  const { userData } = useContext(UserContext);
  const [submitting, setSubmitting] = useState(false);

  const navigate = useNavigate();

  const formik = useFormik({
    validationSchema: createModSchema,
    initialValues: {
      title: '',
      mainCategory: '',
      subCategory: { val: '', disabled: true },
      subSubCategory: { val: '', disabled: true },
      modInfo: '',
      specifications: '',
      mod: null,
      youtubeURL: '',
      mainImage: null,
      images: [],
    },
    onSubmit: async (values) => {
      try {
        if (
          (!values.subCategory.disabled && values.subCategory.val === '') ||
          (!values.subSubCategory.disabled && values.subSubCategory.val === '')
        ) {
          formik.setFieldError('mainCategory', 'Categories are required');
          return;
        }

        setSubmitting(true);
        setDisplayMsg({ opn: true, msg: 'The mod is uploading' });

        const modRef = await createMod(
          userData,
          values.title.trim(),
          values.mainCategory,
          values.subCategory.val,
          values.subSubCategory.val,
          values.modInfo
            .split('\n')
            .map((str) => str.trim())
            .filter((str) => !!str)
            .join('|n|'),
          values.specifications
            .split('\n')
            .map((str) => str.trim())
            .filter((str) => !!str)
            .join('|n|'),
          values.youtubeURL.trim(),
          userData.admin
        );

        const modDownloadUrl = await addModBinaryDataToStorage(values.mod, modRef.key);
        await updateModField(modRef.key, 'binaryContent', modDownloadUrl);
        await updateUserField(userData.username, `mods/${modRef.key}`, true);

        const mainImageDownloadUrl = await addModImageBinaryDataToStorage(
          values.mainImage,
          modRef.key,
          'main-image'
        );
        await updateModField(modRef.key, 'mainImage', mainImageDownloadUrl);

        for (const image of values.images) {
          const imageDownloadUrl = await addModImageBinaryDataToStorage(
            image,
            modRef.key,
            'images'
          );
          await addModImage(modRef.key, imageDownloadUrl);
        }

        // throw new Error('asd');

        setDisplayMsg({ opn: true, msg: 'Mod created successfully!' });

        if (!userData.admin)
          await addMessage(MESSAGE_FOR_APPROVE, userData.username, modRef.key, null, true);

        navigate(`/mod/${modRef.key}`);
      } catch (e) {
        console.error(e);
        setSubmitting(false);
        setDisplayMsg({ opn: true, msg: 'Error ocurred, please try again later.' });
      }
    },
  });

  return (
    <>
      {submitting && <Loading infinite />}
      <form
        onSubmit={formik.handleSubmit}
        style={{
          backgroundColor: '#181C23',
          border: '1px dotted #FD0017',
          padding: '20px 20px 20px 0',
        }}
      >
        <Grid container rowSpacing={4}>
          {/* {TITLE SECTION} */}
          <Grid item xs={6} container alignContent="center" direction="column">
            <h3>Title</h3>
            <StyledTextField
              id="title"
              onChange={(e) => {
                formik.setFieldValue('title', e.target.value);
              }}
              error={formik.errors.title && Boolean(formik.touched.title)}
            />
            <ErrorStyled>{formik.touched.title ? formik.errors.title : ''}</ErrorStyled>
          </Grid>

          {/* {CATEGORIES SECTION} */}
          <Grid item xs={6}>
            <Categories formik={formik} />
          </Grid>

          {/* {MOD INFORMATION SECTION} */}
          <Grid item xs={6} container alignContent="center" direction="column">
            <h3>Mod Information</h3>
            <StyledTextField
              multiline
              id="mod-info"
              rows="10"
              {...formik.getFieldProps('modInfo')}
              error={formik.errors.modInfo && Boolean(formik.touched.modInfo)}
            />
            <ErrorStyled>{formik.touched.modInfo ? formik.errors.modInfo : ''}</ErrorStyled>
          </Grid>

          {/* {SPECIFICATIONS SECTION} */}
          <Grid item xs={6} container alignContent="center" direction="column">
            <h3>Specifications</h3>
            <StyledTextField
              multiline
              id="specifications"
              rows="10"
              {...formik.getFieldProps('specifications')}
              error={formik.errors.specifications && Boolean(formik.touched.specifications)}
            />
            <ErrorStyled>
              {formik.touched.specifications ? formik.errors.specifications : ''}
            </ErrorStyled>
          </Grid>

          {/* {MOD SECTION} */}
          <Grid item xs={6} container spacing={1}>
            <FormFieldMod formik={formik} StyledButton={StyledButton} />
          </Grid>

          {/* {YOUTUBE SECTION} */}
          <Grid item xs={6} container alignContent="center" direction="column">
            <h3>YouTube URL</h3>
            <StyledTextField id="youtube-url" {...formik.getFieldProps('youtubeURL')} />
            <ErrorStyled>{formik.touched.youtubeURL ? formik.errors.youtubeURL : ''}</ErrorStyled>
          </Grid>

          {/* {MAIN IMAGE SECTION} */}
          <Grid item xs={6} container spacing={1}>
            <FormFieldMainImage formik={formik} StyledButton={StyledButton} />
          </Grid>

          {/* {IMAGES SECTION} */}
          <Grid item xs={6} container spacing={1}>
            <FormFieldImages formik={formik} StyledButton={StyledButton} />
          </Grid>

          {/* {UPLOAD BUTTON SECTION} */}
          <Grid item xs={12} container justifyContent="center">
            <StyledButton type="submit" style={{ width: '50%', padding: '20px 0' }}>
              <SendIcon sx={iconsStyle} />
              {userData?.admin ? 'Upload the mod' : 'Send for approval'}
            </StyledButton>
          </Grid>
        </Grid>
      </form>
    </>
  );
}
