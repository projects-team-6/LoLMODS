import { Chip, Grid } from '@mui/material';
import ImageIcon from '@mui/icons-material/Image';
import { ErrorStyled, iconsStyle } from '../../../common/custom-styles/components';

export default function FormFieldMainImage({ formik, StyledButton }) {
  return (
    <>
      <Grid item xs={12} container justifyContent="center">
        <label htmlFor="main-image">
          <input
            type="file"
            id="main-image"
            accept="image/*"
            value=""
            onChange={(e) => {
              formik.setFieldValue('mainImage', e.target.files[0] || null);
            }}
          />
          <StyledButton component="span">
            <ImageIcon sx={iconsStyle} />
            Main Image
          </StyledButton>
        </label>
      </Grid>

      <Grid item xs={12} container justifyContent="center" sx={{ height: '60px' }}>
        {formik.values.mainImage && (
          <Chip
            label={
              formik.values.mainImage.name.length > 20
                ? `${formik.values.mainImage.name.slice(0, 16)}...`
                : formik.values.mainImage.name
            }
            variant="outlined"
            onDelete={() => formik.setFieldValue('mainImage', null)}
          />
        )}
        <ErrorStyled>{formik.touched.mainImage ? formik.errors.mainImage : ''}</ErrorStyled>
      </Grid>
    </>
  );
}
