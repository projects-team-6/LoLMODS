import { Chip, Grid } from '@mui/material';
import DriveFolderUploadOutlinedIcon from '@mui/icons-material/DriveFolderUploadOutlined';
import { ErrorStyled, iconsStyle } from '../../../common/custom-styles/components';

export default function FormFieldMod({ formik, StyledButton }) {
  return (
    <>
      <Grid item xs={12} container justifyContent="center">
        <label htmlFor="mod">
          <input
            type="file"
            id="mod"
            accept=".fantome"
            value=""
            onChange={(e) => {
              formik.setFieldValue('mod', e.target.files[0] || null);
            }}
          />
          <StyledButton component="span">
            <DriveFolderUploadOutlinedIcon sx={iconsStyle} />
            Upload MOD
          </StyledButton>
        </label>
      </Grid>

      <Grid item xs={12} container justifyContent="center" sx={{ height: '60px' }}>
        {formik.values.mod && (
          <Chip
            label={
              formik.values.mod.name.length > 20
                ? `${formik.values.mod.name.slice(0, 16)}...`
                : formik.values.mod.name
            }
            variant="outlined"
            onDelete={() => formik.setFieldValue('mod', null)}
          />
        )}
        <ErrorStyled>{formik.touched.mod ? formik.errors.mod : ''}</ErrorStyled>
      </Grid>
    </>
  );
}
