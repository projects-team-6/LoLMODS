import { Box, Button, ButtonGroup } from '@mui/material';
import { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import NotApproved from '../../components/NotApproved/NotApproved';
import UsersTable from '../../components/UsersTable/UsersTable';
import UserContext from '../../context/UserContext';

const Admin = ({ setDisplayMsg, view = 'users' }) => {
  const { userData } = useContext(UserContext);

  const navigate = useNavigate();

  if (!userData?.admin) return;

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        '& > *': {
          m: 1,
        },
      }}
    >
      <ButtonGroup variant="text" aria-label="text button group" color="inherit">
        <Button
          onClick={() => navigate('/admin', { replace: true })}
          style={{ borderBottom: view === 'users' && '2px solid #FD0017' }}
        >
          Users
        </Button>
        <Button
          onClick={() => navigate('/admin/pending', { replace: true })}
          style={{ borderBottom: view === 'pending' && '2px solid #FD0017' }}
        >
          Pending
        </Button>
      </ButtonGroup>
      {view === 'users' && <UsersTable setDisplayMsg={setDisplayMsg} />}
      {view === 'pending' && <NotApproved />}
    </Box>
  );
};

export default Admin;
