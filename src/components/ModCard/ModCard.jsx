import { Avatar, Grid, Grow, Rating, Stack } from '@mui/material';
import { useEffect, useState } from 'react';
import './ModCard.css';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import { useNavigate } from 'react-router-dom';
import { getAvatar } from '../../services/user.services';

const ModCard = ({ mod }) => {
  const [show, setShow] = useState(false);
  const [avatarUrl, setAvatarUrl] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    getAvatar(mod.author.username).then((url) => {
      setAvatarUrl(url);
    });
  });

  const handelHover = () => {
    setShow(!show);
  };

  const handelClick = (modId) => {
    navigate(`/mod/${modId}`);
  };

  return (
    <Grid item xs={4} onClick={() => handelClick(mod.id)}>
      {show ? (
        <Grow className="card-info" in={show} onMouseLeave={handelHover}>
          <Stack direction="column" alignItems="center" spacing={4}>
            <h2 className={mod.title.length > 20 ? 'smaller-title card-name' : 'card-name'}>
              {mod.title}
            </h2>
            <Rating
              className="card-rating"
              name="read-only"
              precision={0.5}
              value={
                mod.rating
                  ? typeof mod.rating === 'object'
                    ? Object.values(mod.rating).reduce((acc, r) => acc + r, 0) /
                      Object.keys(mod.rating).length
                    : mod.rating
                  : 0
              }
              readOnly
            />
            <Stack direction="row" spacing={1} alignItems="flex-end" className="card-footer">
              <Avatar alt="avatar" src={avatarUrl} sx={{ width: 15, height: 15 }}>
                {!avatarUrl ? mod.author.username[0].toUpperCase() : null}
              </Avatar>
              <span>{`By ${mod.author.username}`}</span>
              <CalendarMonthIcon fontSize="small" />
              <span>
                {new Date(mod.createdOn).toLocaleDateString('en', {
                  month: 'short',
                  day: 'numeric',
                  year: 'numeric',
                })}
              </span>
            </Stack>
          </Stack>
        </Grow>
      ) : (
        <img
          className="card-front"
          src={mod.mainImage}
          alt="img"
          onMouseOver={handelHover}
          style={{ border: '5px solid #FD0017', borderRadius: '5px' }}
        />
      )}
    </Grid>
  );
};

export default ModCard;
