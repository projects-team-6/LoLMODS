import { Box, Grid, Stack } from '@mui/material';
import { DataGrid } from '@mui/x-data-grid';
import { useContext, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { iconsStyle, InitMainButton } from '../../common/custom-styles/components';
import UserContext from '../../context/UserContext';
import { addMessage } from '../../services/message.service';
import { getModById, updateModField } from '../../services/mod.services';
import { getMembers, updateUserField } from '../../services/user.services';
import AddMemberModal from './addMemberModal/addMemberModal';
import PersonAddAlt1Icon from '@mui/icons-material/PersonAddAlt1';

const columns = [
  { field: 'username', headerName: 'Username', width: 140, cellClassName: 'clickable' },
  { field: 'mod', headerName: 'Mod name', width: 220 },
  { field: 'role', headerName: 'Role', width: 160 },
  { field: 'createdOn', headerName: 'User from', width: 130 },
  { field: 'lastActivity', headerName: 'Last activity', width: 120 },
  { field: 'remove', headerName: '', width: 120, cellClassName: 'clickable' },
];
const StyledButton = InitMainButton();

const Members = ({ setDisplayMsg }) => {
  const { userData, setContext } = useContext(UserContext);
  const { modId } = useParams();
  const [rows, setRows] = useState([]);
  const [open, setOpen] = useState(false);

  const navigate = useNavigate();

  useEffect(() => {
    if (!Object.keys(userData.mods).includes(modId)) {
      return navigate(`/mod/${modId}`);
    }
  }, []);

  useEffect(() => {
    getModById(modId).then((mod) => {
      return getMembers(modId).then((users) => {
        setRows(
          users.map((user) => {
            return {
              id: user.uid,
              username: user.username,
              mod: mod.title,
              role: user.uid === mod.author.uid ? 'Owner' : 'Maintainer',
              createdOn: user.createdOn.toLocaleDateString(),
              lastActivity: mod?.lastActivity?.[user.username]
                ? new Date(mod?.lastActivity?.[user.username]).toLocaleDateString()
                : 'Never',
              remove:
                userData.uid === user.uid && user.uid !== mod.author.uid
                  ? 'Leave'
                  : user.uid !== mod.author.uid && userData.uid === mod.author.uid
                  ? 'Remove'
                  : '',
            };
          })
        );
      });
    });
  }, [open]);

  const handelAddUser = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handelClick = (params) => {
    const target = params.field;
    const data = { ...params.row };

    switch (target) {
      case 'username':
        navigate(`/profile/${data.username}`);
        break;
      case 'remove':
        if (data.role !== 'Owner') {
          updateUserField(data.username, `mods/${modId}`, null)
            .then(() => {
              return updateModField(modId, `maintainer/${data.username}`, null).then(() => {
                if (data.remove === 'Leave') {
                  setDisplayMsg({ opn: true, msg: `You leave the mod.` });
                  setContext({
                    user: userData.username,
                    userData: { ...userData, mods: { ...userData.mods, [modId]: null } },
                  });
                  return navigate(`/mod/${modId}`);
                }
                setDisplayMsg({ opn: true, msg: `${data.username} is removed from the mod.` });
                addMessage(
                  'You was removed as a maintainer to a mod.',
                  userData?.username,
                  modId,
                  data.username
                );
                setRows([...rows.filter((u) => u.username !== data.username)]);
              });
            })
            .catch(console.error);
        }

        break;
      default:
        break;
    }
  };

  return (
    <>
      <Grid container rowSpacing={3}>
        <Grid item xs={12}>
          <Stack direction="row">
            <StyledButton onClick={() => navigate(-1)}>Go Back</StyledButton>
            <Box component="span" sx={{ flexGrow: 1 }}></Box>
            <StyledButton onClick={handelAddUser}>
              <PersonAddAlt1Icon sx={iconsStyle} />
              Add user
            </StyledButton>
          </Stack>
        </Grid>
        <Grid item container xs={12} justifyContent="center">
          <div style={{ height: 400, width: '80%' }}>
            <DataGrid
              onCellClick={handelClick}
              rows={rows}
              columns={columns}
              pageSize={5}
              rowsPerPageOptions={[5]}
              disableSelectionOnClick
              sx={{
                boxShadow: 2,
                '& .MuiDataGrid-cell:hover': {
                  color: 'primary.main',
                },
              }}
            />
          </div>
        </Grid>
      </Grid>
      {open && (
        <AddMemberModal
          open={open}
          handleClose={handleClose}
          setDisplayMsg={setDisplayMsg}
          modId={modId}
        />
      )}
    </>
  );
};

export default Members;
