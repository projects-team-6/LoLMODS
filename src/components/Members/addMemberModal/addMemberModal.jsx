import './addMemberModal.css';
import { Modal } from '@mui/material';
import { Box } from '@mui/system';
import { useContext, useEffect, useState } from 'react';
import {
  modalStyle,
  InitMainAutocomplete,
  InitMainButton,
  InitMainTextField,
} from '../../../common/custom-styles/components';
import UserContext from '../../../context/UserContext';
import { addMessage } from '../../../services/message.service';
import { getAllUsers, updateUserField } from '../../../services/user.services';
import { ErrorStyled } from './../../../common/custom-styles/components';
import { updateModField } from '../../../services/mod.services';

const StyledButton = InitMainButton({ padding: '10px 10px' });
const StyledTextField = InitMainTextField({ width: '100%' });
const StyledAutocomplete = InitMainAutocomplete();

const AddMemberModal = ({ open, handleClose, setDisplayMsg, modId }) => {
  const { userData } = useContext(UserContext);
  const [users, setUsers] = useState([]);
  const [errorMsg, setErrorMsg] = useState('');

  useEffect(() => {
    getAllUsers().then((allUsers) => {
      const result = allUsers
        .filter((user) => !user?.mods?.[modId])
        .map((user) => {
          return {
            label: user.username,
          };
        });
      setUsers(result);
    });
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    const username = e.target[0].value;
    if (username === '') return setErrorMsg('User is required');
    updateUserField(username, `mods/${modId}`, true).then(() => {
      return updateModField(modId, `maintainer/${username}`, true)
    }).then(() => {
      addMessage('You was added as a maintainer to a mod.', userData?.username, modId, username);
      setDisplayMsg({ opn: true, msg: `${username} is added to the mod.` });
      handleClose();
    }).catch(console.error);
  };
  return (
    <Modal open={open} onClose={handleClose}>
      <form onSubmit={handleSubmit}>
        <Box sx={modalStyle}>
          <h2 style={{ paddingBottom: '35px' }}>Add User</h2>
          <StyledAutocomplete
            disablePortal
            id="userSelect"
            options={users}
            renderInput={(params) => <StyledTextField {...params} />}
          />
          <ErrorStyled>{errorMsg}</ErrorStyled>
          <Box pt="35px">
            <StyledButton type="submit">Save</StyledButton>
          </Box>
        </Box>
      </form>
    </Modal>
  );
};

export default AddMemberModal;
