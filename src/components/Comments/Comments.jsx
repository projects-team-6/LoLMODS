import { Avatar, Box, Grid, IconButton, Stack, Tooltip } from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import { CommentItem, InitMainTextField } from '../../common/custom-styles/components';
import UserContext from '../../context/UserContext';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import DeleteIcon from '@mui/icons-material/Delete';
import { addComment, deleteComment, getLiveCommentsById } from '../../services/comments.service';
import { getAvatar, updateUserField } from '../../services/user.services';
import { updateModField } from '../../services/mod.services';
import EditCommentForm from './EditCommentForm/EditCommentForm';
import { useNavigate } from 'react-router-dom';
import CustomPagination from '../CustomPagination/CustomPagination';

const StyledTextField = InitMainTextField({ width: '100%' });
const Item = CommentItem();

const Comments = ({ modId, setDisplayMsg }) => {
  const { userData } = useContext(UserContext);
  const [comments, setComments] = useState([]);
  const [openEdit, setOpenEdit] = useState(false);
  const [currentComment, setCurrentComment] = useState(null);

  const navigate = useNavigate();

  useEffect(() => {
    const unsubscribe = getLiveCommentsById(async (snapshot) => {
      if (!snapshot.exists()) return setComments([]);

      const rawData = snapshot.val();
      const com = await Promise.all(
        Object.keys(rawData).map(async (key) => {
          const userAvatar = await getAvatar(rawData[key].author);
          return {
            ...rawData[key],
            createdOn: new Date(rawData[key].createdOn),
            id: key,
            userAvatar,
          };
        })
      );
      setComments([...com].sort((a, b) => b.createdOn - a.createdOn));
    }, modId);
    return () => unsubscribe;
  }, []);

  const handelComment = (e) => {
    if (e.key !== 'Enter') return;
    const text = e.target.value;
    if (text === '') return;
    e.target.value = '';

    if (!userData) {
      setDisplayMsg({ opn: true, msg: 'You should log in first!'})
      navigate('/login');
      return;
    }
    if (userData?.blocked) {
      setDisplayMsg({ opn: true, msg: 'Sorry! You are blocked!'})
      return;
    }

    addComment(text, userData.username, userData.uid, modId)
      .then((c) => {
        return Promise.all([
          updateUserField(userData.username, `comments/${c.id}`, true),
          updateModField(modId, `comments/${c.id}`, true),
        ]);
      })
      .then(() => {
        setDisplayMsg({ opn: true, msg: 'Commend added.' });
      })
      .catch(console.log);
  };

  const handelEditComment = (comment) => {
    setCurrentComment(comment);
    setOpenEdit(true);
  };

  const handleClose = () => {
    setOpenEdit(false);
  };

  const handelDeleteComment = (commentId, username) => {
    deleteComment(commentId)
      .then(() => {
        return Promise.all([
          updateUserField(username, `comments/${commentId}`, null),
          updateModField(modId, `comments/${commentId}`, null),
        ]);
      })
      .then(() => {
        setDisplayMsg({ opn: true, msg: 'Comment deleted.' });
      })
      .catch(console.error);
  };

  return (
    <>
      <Grid container className="comment_block" rowSpacing={2} columnGap={4}>
        <Grid item xs={12}>
          <Stack direction="row" columnGap={4} sx={{ marginTop: '15px' }}>
            <Avatar alt="avatar" src={userData?.avatarUrl} sx={{ width: 55, height: 55 }}>
              {!userData?.avatarUrl ? userData?.username[0].toUpperCase() : null}
            </Avatar>
            <StyledTextField
              id="comments"
              autoComplete="off"
              placeholder="Join the conversation.."
              onKeyUp={handelComment}
            />
          </Stack>
        </Grid>
        {comments.length > 0 ? (
          <CustomPagination itemsPerPage={6}>
            {comments.map((comment) => {
              return (
                <Grid item xs={12} key={comment?.id}>
                  <Stack direction="row" columnGap={4} sx={{ marginTop: '15px' }}>
                    <Avatar alt="avatar" src={comment?.userAvatar} sx={{ width: 55, height: 55 }}>
                      {!comment?.userAvatar ? comment?.author?.[0].toUpperCase() : null}
                    </Avatar>
                    <Stack direction="column" sx={{ flexGrow: 1, marginBottom: '5px' }}>
                      <Item>{comment?.content}</Item>
                      <Stack direction="row">
                        <AccessTimeIcon fontSize="small" />
                        <Box component="span" pr={2}>
                          {comment?.createdOn?.toLocaleTimeString([], {
                            hour: '2-digit',
                            minute: '2-digit',
                          })}
                        </Box>
                        <CalendarMonthIcon fontSize="small" />
                        <Box component="span" pr={2}>
                          {comment?.createdOn?.toLocaleDateString('en-GB', {
                            day: '2-digit',
                            month: '2-digit',
                            year: 'numeric',
                          })}
                        </Box>
                        <PersonOutlineIcon fontSize="small" />
                        <Box
                          component="span"
                          sx={{ cursor: 'pointer' }}
                          onClick={() => navigate(`/profile/${comment?.author}`)}
                        >
                          {comment?.author}
                        </Box>
                        <Box sx={{ flexGrow: 1 }} />
                        {userData?.username === comment?.author ? (
                          <>
                            <Tooltip title="edit comment">
                              <IconButton onClick={() => handelEditComment(comment)}>
                                <ModeEditIcon fontSize="small" />
                              </IconButton>
                            </Tooltip>
                            <Tooltip title="delete comment">
                              <IconButton
                                onClick={() => handelDeleteComment(comment.id, comment.author)}
                              >
                                <DeleteIcon fontSize="small" />
                              </IconButton>
                            </Tooltip>
                          </>
                        ) : userData?.admin ? (
                          <Tooltip title="delete comment">
                            <IconButton
                              onClick={() => handelDeleteComment(comment.id, comment.author)}
                            >
                              <DeleteIcon fontSize="small" />
                            </IconButton>
                          </Tooltip>
                        ) : null}
                      </Stack>
                    </Stack>
                  </Stack>
                </Grid>
              );
            })}
          </CustomPagination>
        ) : null}
      </Grid>
      {openEdit && (
        <EditCommentForm
          openEdit={openEdit}
          currentComment={currentComment}
          handleClose={handleClose}
          setDisplayMsg={setDisplayMsg}
        />
      )}
    </>
  );
};

export default Comments;
