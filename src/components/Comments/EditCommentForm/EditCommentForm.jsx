import { Modal } from '@mui/material';
import {
  modalStyle,
  ErrorStyled,
  InitMainButton,
  InitMainTextField,
} from '../../../common/custom-styles/components';
import { useFormik } from 'formik';
import { Box } from '@mui/system';
import { editCommentSchema } from '../../../common/form-schemas/modSchemas';
import { updateComment } from '../../../services/comments.service';

const StyledButton = InitMainButton({ padding: '10px 10px' });
const StyledTextField = InitMainTextField({ width: '100%' });

const EditCommentForm = ({ currentComment, openEdit, handleClose, setDisplayMsg }) => {
  const formik = useFormik({
    initialValues: {
      comment: currentComment.content,
    },
    validationSchema: editCommentSchema,
    onSubmit: (values) => {
      updateComment(currentComment?.id, `content`, values.comment)
        .then(() => {
          handleClose();
          setDisplayMsg({ opn: true, msg: 'Comment updated.' });
        })
        .catch(console.log);
    },
  });

  return (
    <Modal open={openEdit} onClose={handleClose}>
      <form onSubmit={formik.handleSubmit}>
        <Box sx={modalStyle}>
          <h2 style={{ paddingBottom: '35px' }}>Edit comment</h2>
          <StyledTextField
            id="comment"
            name="comment"
            autoComplete="off"
            value={formik.values.comment}
            onChange={formik.handleChange}
            error={formik.touched.comment && Boolean(formik.errors.comment)}
          />
          <ErrorStyled>{formik.errors.comment}</ErrorStyled>
          <Box pt="35px">
            <StyledButton type="submit">Save</StyledButton>
          </Box>
        </Box>
      </form>
    </Modal>
  );
};

export default EditCommentForm;
