import Snackbar from '@mui/material/Snackbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { useEffect, useState } from 'react';

export default function Notify({ opn, msg, setDisplayMsg }) {
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setOpen(opn);
  }, [opn]);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setDisplayMsg({ opn: false, msg: '' });
    setOpen(false);
  };

  const action = (
    <>
      <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
        <CloseIcon fontSize="small" />
      </IconButton>
    </>
  );

  return (
    <div>
      <Snackbar
        open={open}
        autoHideDuration={3000}
        onClose={handleClose}
        message={msg}
        action={action}
        style={{ zIndex: 100000 }}
      />
    </div>
  );
}
