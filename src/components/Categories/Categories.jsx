import { Grid, ThemeProvider } from '@mui/material';
import { categories } from '../../common/categories';
import {
  ErrorStyled,
  InitMainMenuItem,
  InitMainSelect,
} from '../../common/custom-styles/components';
import { categoriesTheme } from '../../common/custom-styles/themes';

const StyledSelect = InitMainSelect({ width: '100%' });
const StyledMenuItem = InitMainMenuItem();

export default function Categories({ formik }) {
  return (
    <>
      <ThemeProvider theme={categoriesTheme}>
        <Grid container columnSpacing={2}>
          <Grid item xs={4}>
            <h3>Main category</h3>
            <StyledSelect
              name="main-category"
              id="main-category"
              className="category"
              value={formik.values.mainCategory}
              onChange={(e) => {
                formik.setFieldValue('mainCategory', e.target.value);
                if (categories[e.target.value])
                  formik.setFieldValue('subCategory', { val: '', disabled: false });
                else formik.setFieldValue('subCategory', { val: '', disabled: true });
                formik.setFieldValue('subSubCategory', { val: '', disabled: true });
              }}
              error={formik.errors.mainCategory && Boolean(formik.touched.mainCategory)}
            >
              {Object.keys(categories).map((category) => (
                <StyledMenuItem key={category} value={category}>
                  {category}
                </StyledMenuItem>
              ))}
            </StyledSelect>
          </Grid>

          <Grid item xs={4}>
            <h3>Sub category</h3>
            <StyledSelect
              name="sub-category"
              id="sub-category"
              className="category"
              value={formik.values.subCategory.val}
              disabled={formik.values.subCategory.disabled}
              onChange={(e) => {
                formik.setFieldValue('subCategory', { val: e.target.value, disabled: false });
                if (categories[formik.values.mainCategory][e.target.value])
                  formik.setFieldValue('subSubCategory', { val: '', disabled: false });
                else formik.setFieldValue('subSubCategory', { val: '', disabled: true });
              }}
            >
              {!formik.values.subCategory.disabled &&
                Object.keys(categories[formik.values.mainCategory]).map((category) => (
                  <StyledMenuItem key={category} value={category}>
                    {category}
                  </StyledMenuItem>
                ))}
            </StyledSelect>
          </Grid>

          <Grid item xs={4}>
            <h3>Sub-sub category</h3>
            <StyledSelect
              name="sub-sub-caregory"
              id="sub-sub-caregory"
              className="category"
              value={formik.values.subSubCategory.val}
              disabled={formik.values.subSubCategory.disabled}
              onChange={(e) => {
                formik.setFieldValue('subSubCategory', { val: e.target.value, disabled: false });
              }}
            >
              {!formik.values.subSubCategory.disabled &&
                Object.keys(
                  categories[formik.values.mainCategory][formik.values.subCategory.val]
                ).map((category) => (
                  <StyledMenuItem key={category} value={category}>
                    {category}
                  </StyledMenuItem>
                ))}
            </StyledSelect>
          </Grid>
        </Grid>
        <ErrorStyled>{formik.touched.mainCategory ? formik.errors.mainCategory : ''}</ErrorStyled>
      </ThemeProvider>
    </>
  );
}
