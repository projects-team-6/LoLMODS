import {
  IconButton,
  Badge,
  List,
  ListItemText,
  Divider,
  ListItemButton,
  Popover,
  ListItemIcon,
} from '@mui/material';
import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import { Fragment, useContext, useEffect, useState } from 'react';
import UserContext from '../../context/UserContext';
import {
  deleteMessage,
  fromMessagesDocument,
  getAllAdminMessages,
  getAllMessages,
} from '../../services/message.service';
import AddTaskIcon from '@mui/icons-material/AddTask';
import { useNavigate } from 'react-router-dom';

const Inbox = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [messages, setMessages] = useState([]);
  const [userMessages, setUserMessages] = useState([]);
  const [adminMessages, setAdminMessages] = useState([]);
  const { userData } = useContext(UserContext);

  const navigate = useNavigate();

  useEffect(() => {
    const unsubscribe = getAllMessages((snapshot) => {
      const rawData = fromMessagesDocument(snapshot);
      const msgs = Array.from(Object.values(rawData)).sort((a, b) => b.createdOn - a.createdOn);
      setUserMessages([...msgs]);
    }, userData.username);
    return () => unsubscribe();
  }, []);

  useEffect(() => {
    if (!userData?.admin) return;
    const unsubscribe = getAllAdminMessages((snapshot) => {
      const rawData = fromMessagesDocument(snapshot);
      const msgs = Array.from(Object.values(rawData)).sort((a, b) => b.createdOn - a.createdOn);
      setAdminMessages([...msgs]);
    });
    return () => unsubscribe();
  }, []);

  useEffect(() => {
    const msg = [...adminMessages, ...userMessages].sort((a, b) => b.createdOn - a.createdOn);

    setMessages([...msg]);
  }, [adminMessages, userMessages]);

  const handelClick = (e) => {
    setAnchorEl(e.target);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handelMessageClick = (msgId, modId = null) => {
    deleteMessage(msgId).then(() => {
      handleClose();
      if (modId) {
        navigate(`/mod/${modId}`);
      }
    });
  };

  return (
    <>
      <IconButton
        aria-describedby="notification"
        aria-controls="notification"
        aria-haspopup="true"
        onClick={handelClick}
      >
        <Badge badgeContent={messages.filter((m) => m.new === true).length} color="error">
          <NotificationsNoneIcon color="secondary" />
        </Badge>
      </IconButton>
      {messages.length > 0 ? (
        <Popover
          id="notification"
          open={Boolean(anchorEl)}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'left',
          }}
        >
          <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'rgba(121,9,17,1)' }}>
            {messages?.map((m, i) => {
              return (
                <Fragment key={i}>
                  <ListItemButton onClick={() => handelMessageClick(m.id, m.modId)}>
                    <ListItemIcon sx={{ pr: 2 }}>
                      <AddTaskIcon />
                    </ListItemIcon>
                    <ListItemText
                      primary={m.content}
                      secondary={m.createdOn.toLocaleDateString('en', {
                        month: 'short',
                        day: 'numeric',
                        year: 'numeric',
                      })}
                    />
                  </ListItemButton>
                  {i < messages.length - 1 ? <Divider style={{ background: 'white' }} /> : null}
                </Fragment>
              );
            })}
          </List>
        </Popover>
      ) : null}
    </>
  );
};

export default Inbox;
