import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import { useState } from 'react';

export default function Loading({ children, infinite = false }) {
  const [open, setOpen] = useState(true);

  if (!infinite) {
    setTimeout(() => {
      setOpen(false);
    }, 700);
  }

  return (
    <div>
      <Backdrop sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 9999 }} open={open}>
        <CircularProgress color="inherit" />
      </Backdrop>
      {open ? null : children}
    </div>
  );
}
