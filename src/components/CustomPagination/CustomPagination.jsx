import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import { useEffect, useState } from 'react';
import { Grid } from '@mui/material';
import './CustomPagination.css';

export default function CustomPagination( { itemsPerPage=6, children } ) {
  
  const [itemsToShow, setItemsToShow] = useState([])
  const [page, setPage] = useState(1);
  
  useEffect(() => {
    const start = (page-1)*itemsPerPage;
    const end = (page-1)*itemsPerPage + itemsPerPage;
    setItemsToShow(children.slice(start, end))
  }, [page, children])

  useEffect(()=> {
    setPage(1)
  },[children])
  
  const handleChange = (event, value) => {
    setPage(value);
  };

  return (
    <>
      
        {itemsToShow} 

      {Math.ceil(children.length/itemsPerPage) > 1 &&
      <Grid item xs={12}>
      <Stack spacing={2} mt={5} alignItems='center'>
        <Pagination 
          // color="secondary" 
          variant="outlined" 
          shape="rounded" 
          count={Math.ceil(children.length/itemsPerPage)} 
          page={page} 
          onChange={handleChange} 
        />
      </Stack>
      </Grid>
      }
    </>
  );
}