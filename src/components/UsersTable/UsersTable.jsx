import { Rating, Tooltip } from '@mui/material';
import { DataGrid } from '@mui/x-data-grid';
import { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import UserContext from '../../context/UserContext';
import { addMessage } from '../../services/message.service';
import { getApprovedMods } from '../../services/mod.services';
import { getAllUsers, updateUserField } from '../../services/user.services';
import './UsersTable.css';

const columns = [
  { field: 'username', headerName: 'Username', width: 140, cellClassName: 'clickable' },
  { field: 'fullName', headerName: 'Full name', width: 160 },
  { field: 'email', headerName: 'Email', width: 160 },
  { field: 'phone', headerName: 'Phone number', width: 130 },
  { field: 'mods', headerName: 'Mods', width: 120 },
  { field: 'downloads', headerName: 'Downloads', width: 120 },
  {
    field: 'rating',
    headerName: 'Rating',
    width: 180,
    renderCell: (cellValues) => {
      return (
        <Tooltip title={cellValues.value.toString()}>
          <Rating name="read-only" value={+cellValues.value} precision={0.5} readOnly />
        </Tooltip>
      );
    },
  },
  { field: 'status', headerName: 'Status', width: 120, cellClassName: 'clickable' },
];

const UsersTable = ({ setDisplayMsg }) => {
  const {
    userData: { uid, username },
  } = useContext(UserContext);
  const [rows, setRows] = useState([]);

  const navigate = useNavigate();

  useEffect(() => {
    getAllUsers()
      .then(async (users) => {
        const rawMods = await getApprovedMods();

        return users
          .map((u) => {
            let modData ;
            if (rawMods.exists()) {
              modData = Object.values(rawMods.val())
                .filter((m) => (m.author.uid === u.uid))
                .reduce(
                  (acc, d) => {
                    acc.downloads += d.downloads ? d.downloads : 0;
                    acc.rating = d.rating
                      ? [...acc.rating, ...Object.values(d.rating)]
                      : [...acc.rating];
                    return acc;
                  },
                  { downloads: 0, rating: [] }
                );
            }

            modData.rating =
              modData.rating.length > 0
                ? modData.rating.reduce((acc, r) => acc + r, 0) / modData.rating.length
                : 0;

            return {
              id: u.uid,
              username: u.username,
              fullName: u.fullName,
              email: u.email,
              phone: u.phoneNumber,
              mods: u.mods ? Object.keys(u.mods).length : 0,
              downloads: modData.downloads,
              rating: modData.rating,
              status: u.admin ? 'ADMIN' : u.blocked ? 'BLOCKED' : 'ACTIVE',
            };
          })
          .filter((u) => u.id !== uid);
      })
      .then((users) => {
        setRows(users);
      })
      .catch(console.log);
  }, []);

  const handelClick = (params, event) => {
    const target = params.field;
    const data = { ...params.row };

    if (target === 'username') {
      navigate(`/profile/${data.username}`);
    }
    if (target === 'status') {
      if (data.status === 'ADMIN') return;
      const block = data.status === 'ACTIVE' ? true : null;
      params.row.status = block ? 'BLOCKED' : 'ACTIVE';
      updateUserField(data.username, 'blocked', block)
        .then(() => {
          setDisplayMsg({
            opn: true,
            msg: `${data.username} was ${block ? 'Blocked' : 'Unblocked'}`,
          });
          return addMessage(
            `Your profile was ${block ? 'Blocked' : 'Unblocked'}`,
            username,
            null,
            data.username
          );
        })
        .catch(console.log);
    }
  };

  return (
    <div style={{ height: 400, width: '100%' }}>
      <DataGrid
        onCellClick={handelClick}
        rows={rows}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
        disableSelectionOnClick
        sx={{
          boxShadow: 2,
          '& .MuiDataGrid-cell:hover': {
            color: 'primary.main',
          },
        }}
        // checkboxSelection
      />
    </div>
  );
};

export default UsersTable;
