import { Box, AppBar, MenuItem, IconButton, Menu, Toolbar, Avatar } from '@mui/material';
import './Header.css';
import Typography from '@mui/material/Typography';
import { ThemeProvider } from '@mui/material/styles';
import { useContext, useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import UserContext from '../../context/UserContext';
import { logoutUser } from '../../services/auth.service';
import Inbox from '../Inbox/Inbox';
import { headerTheme } from '../../common/custom-styles/themes';
import { iconsStyle, InitMainButton } from '../../common/custom-styles/components';
import LoginIcon from '@mui/icons-material/Login';
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import EditIcon from '@mui/icons-material/Edit';
import LogoutIcon from '@mui/icons-material/Logout';

const StyledButton = InitMainButton();

const Header = ({ setDisplayMsg }) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const { user, userData, setContext } = useContext(UserContext);

  const navigate = useNavigate();

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (event) => {
    const targetId = event.target.id;
    if (targetId === 'logout') {
      logoutUser();
      setContext(null);
      setDisplayMsg({ opn: true, msg: 'Log out successfully!' });
      navigate('/');
    }
    if (targetId === 'edit') {
      navigate('/edit');
    }
    if (targetId === 'profile') {
      navigate(`/profile/${user}`);
    }
    if (targetId === 'admin') {
      navigate('/admin');
    }
    setAnchorEl(null);
  };

  return (
    <ThemeProvider theme={headerTheme}>
      <AppBar position="static">
        <Toolbar sx={{ justifyContent: 'space-between' }}>
          <Typography
            href="/"
            align="left"
            textAlign="center"
            variant="h4"
            component="a"
            sx={{ color: '#fff', textDecoration: 'none' }}
            onClick={(e) => {
              e.preventDefault();
              navigate('/');
            }}
          >
            <img className="logo-img" src={process.env.PUBLIC_URL + '/LoLMODS.png'} alt="Logo" />
            LoLMODS
          </Typography>

          <Box>
            <StyledButton
              variant="h6"
              component="a"
              onClick={() => {
                if (userData?.blocked) {
                  setDisplayMsg({ opn: true, msg: 'Sorry! You are blocked!'})
                  return;
                }
                navigate('/create-mod')
              } }
              style={{ marginRight: '15px' }}
            >
              Create Mod
            </StyledButton>

            <StyledButton
              variant="h6"
              component="a"
              onClick={() =>
                navigate(`/mods?searchParam=&mainCat=All&subCat=All&subSubCat=All&sort=New`)
              }
              style={{ marginRight: '15px' }}
            >
              All Mods
            </StyledButton>

            <StyledButton variant="h6" component="a" onClick={() => navigate('/how-to-install')}>
              How to install
            </StyledButton>
          </Box>

          {user ? (
            <div>
              <Inbox />
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
              >
                <Avatar alt="avatar" src={userData?.avatarUrl} sx={{ width: 40, height: 40 }}>
                  {!userData?.avatarUrl ? user[0].toUpperCase() : null}
                </Avatar>
                {/* <AccountCircle /> */}
              </IconButton>
              <Menu
                className="user-menu"
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'left',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'center',
                }}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                {userData?.admin ? (
                  <MenuItem id="admin" onClick={handleClose}>
                    <AdminPanelSettingsIcon sx={iconsStyle} />
                    Admin
                  </MenuItem>
                ) : null}
                <MenuItem id="profile" onClick={handleClose}>
                  <AccountBoxIcon sx={iconsStyle} />
                  Profile
                </MenuItem>
                <MenuItem id="edit" onClick={handleClose}>
                  <EditIcon sx={iconsStyle} />
                  Edit
                </MenuItem>
                <MenuItem id="logout" onClick={handleClose}>
                  <LogoutIcon sx={iconsStyle} />
                  Log out
                </MenuItem>
              </Menu>
            </div>
          ) : (
            <NavLink to={'login'} style={{ textDecoration: 'none' }}>
              <StyledButton>
                <LoginIcon sx={iconsStyle} />
                Login
              </StyledButton>
            </NavLink>
          )}
        </Toolbar>
      </AppBar>
    </ThemeProvider>
  );
};

export default Header;
