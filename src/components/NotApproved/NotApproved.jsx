import { Grid } from '@mui/material';
import { useEffect, useState } from 'react';
import { getAllMods } from '../../services/mod.services';
import CustomPagination from '../CustomPagination/CustomPagination';
import ModCard from '../ModCard/ModCard';

const NotApproved = () => {
  const [mods, setMods] = useState(null);
  useEffect(() => {
    getAllMods().then((snapshot) => {
      if (!snapshot.exists()) return;

      const rawData = snapshot.val();
      const data = Object.keys(rawData)
        .filter((key) => (rawData[key].newVersion || !rawData[key].approved ? true : false))
        .map((key) => {
          return {
            ...rawData[key],
            id: key,
          };
        });

      setMods([...data]);
    });
  }, []);
  return (
    <Grid container spacing={2}>
      {mods?.length > 0 ? (
        <CustomPagination>
          {mods.map((mod) => {
            return <ModCard mod={mod} key={mod.id} />;
          })}
        </CustomPagination>
      ) : (
        <h2>Nothing Here</h2>
      )}
    </Grid>
  );
};

export default NotApproved;
