import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { getStorage } from 'firebase/storage';

export const firebaseConfig = {

  apiKey: "AIzaSyA6BT2mRaLpy-nZY2tZAgfF40nDkw_D8oA",

  authDomain: "addonis-6.firebaseapp.com",

  databaseURL: "https://addonis-6-default-rtdb.europe-west1.firebasedatabase.app",

  projectId: "addonis-6",

  storageBucket: "gs://addonis-6.appspot.com",

  messagingSenderId: "737488701518",

  appId: "1:737488701518:web:b6dab3dff01a2ba3649b1f"

};


export const app = initializeApp(firebaseConfig);
// the Firebase authentication handler
export const auth = getAuth(app);
// the Realtime Database handler
export const db = getDatabase(app);

export const storage = getStorage();
