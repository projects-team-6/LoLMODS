import { ref, push, get, query, equalTo, orderByChild, update, onValue } from 'firebase/database';
import { db } from '../config/firebase-config';

export const fromMessagesDocument = (snapshot) => {
  const messagesDocument = snapshot.val();

  if (!messagesDocument) return {};
  return Object.keys(messagesDocument).map((key) => {
    const message = messagesDocument[key];

    return {
      ...message,
      id: key,
      createdOn: new Date(message.createdOn),
    };
  });
};

export const getMessageById = (id) => {
  return get(ref(db, `messages/${id}`)).then((result) => {
    if (!result.exists()) {
      throw new Error(`Message with id ${id} does not exist!`);
    }

    const message = result.val();
    message.id = id;
    message.createdOn = new Date(message.createdOn);

    return message;
  });
};

export const getAllMessages = (listen, username) => {
  return onValue(query(ref(db, 'messages'), orderByChild('receiver'), equalTo(username)), listen);
};

export const getAllAdminMessages = (listen) => {
  return onValue(query(ref(db, 'messages'), orderByChild('admin'), equalTo(true)), listen);
}

export const addMessage = (content, from, modId, to = null, admin = null) => {
  return push(ref(db, 'messages'), {
    content,
    author: from,
    receiver: to,
    modId: modId,
    createdOn: Date.now(),
    new: true,
    admin,
  }).then((result) => {
    return getMessageById(result.key);
  });
};

export const deleteMessage = (id) => {
  return update(ref(db), {
    [`/messages/${id}`]: null,
  });
};
