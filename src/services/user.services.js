import { get, set, ref, query, equalTo, orderByChild, update } from 'firebase/database';
import { auth, db, storage } from '../config/firebase-config';
import { ref as storageRef, uploadBytes, getDownloadURL, deleteObject } from 'firebase/storage';
import {
  EmailAuthProvider,
  reauthenticateWithCredential,
  updateEmail,
  updatePassword,
} from 'firebase/auth';

export const fromUsersDocument = (snapshot) => {
  const UsersDocument = snapshot.val();

  return Object.keys(UsersDocument).map((key) => {
    const user = UsersDocument[key];

    return {
      ...user,
      id: key,
      createdOn: new Date(user.createdOn),
    };
  });
};

export const getUserByUsername = (username) => {
  return get(ref(db, `users/${username}`));
};

export const getAllUsers = () => {
  return get(ref(db, 'users')).then((snapshot) => {
    if (!snapshot.exists()) {
      return [];
    }
    return fromUsersDocument(snapshot);
  });
};

export const getUserData = (uid) => {
  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};

export const getMembers = (modId) => {
  return getAllUsers().then((users) => {
    return users.filter((user) => user?.mods?.[modId]);
  });
};

export const getAvatar = (username) => {
  return get(ref(db, `users/${username}/avatarUrl`)).then((snapshot) => {
    if (!snapshot.exists()) {
      return null;
    }
    return snapshot.val();
  });
};

export const createUserUsername = (username, uid, email, phoneNumber) => {
  return set(ref(db, `users/${username}`), {
    username,
    uid,
    email,
    phoneNumber,
    createdOn: Number(new Date()),
  });
};

export const uploadUserProfilePicture = (username, file) => {
  const picture = storageRef(storage, `images/${username}/avatar`);

  return uploadBytes(picture, file)
    .then((snapshot) => {
      return getDownloadURL(snapshot.ref).then((url) => {
        return updateUserProfilePicture(username, url).then(() => {
          return url;
        });
      });
    })
    .catch(console.error);
};

export const updateUserProfilePicture = (username, url) => {
  return update(ref(db), {
    [`users/${username}/avatarUrl`]: url,
  });
};

export const updateUserData = (username, email, value) => {
  return updateEmail(auth.currentUser, email).then(() => {
    return getUserByUsername(username)
      .then((snapshot) => snapshot.val())
      .then((data) => {
        return update(ref(db), {
          [`users/${username}`]: { ...data, ...value },
        });
      });
  });
};

export const updateUserPassword = (oldPassword, newPassword) => {
  const user = auth.currentUser;
  const credential = EmailAuthProvider.credential(user.email, oldPassword);

  return reauthenticateWithCredential(user, credential)
    .then(() => {
      return updatePassword(user, newPassword);
    })
    .catch(console.error);
};

export const updateUserField = (username, field, value) => {
  return update(ref(db), {
    [`users/${username}/${field}`]: value,
  });
};

export const removeAvatar = (username) => {
  const pictureRef = storageRef(storage, `images/${username}/avatar`);
  return deleteObject(pictureRef)
    .then(() => {
      return update(ref(db), {
        [`users/${username}/avatarUrl`]: null,
      });
    })
    .catch(console.error);
};
