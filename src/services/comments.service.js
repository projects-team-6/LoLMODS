import { get, push, ref, query, equalTo, orderByChild, update, onValue } from 'firebase/database';
import { db } from '../config/firebase-config';

export const addComment = (content, author, authorId, modId) => {

  return push(
    ref(db, 'comments'),
    {
      content,
      author,
      authorId,
      modId,
      createdOn: Date.now(),
    }
  ).then(result => {
    return getCommentById(result.key);
  })
};

const getCommentById = (id) => {
  return get(ref(db, `comments/${id}`))
  .then(result => {
    if (!result.exists()) {
      throw new Error(`Comment with id ${id} does not exist!`);
    }

    const comment = result.val();
    comment.id = id;
    comment.createdOn = new Date(comment.createdOn);

    return comment;

  })

}

export const getLiveCommentsById = (listen, modId) => {
  return onValue(query(ref(db, 'comments'), orderByChild('modId'), equalTo(modId)), listen);
};


export const updateComment = (commentId, field, value) => {
  return update(ref(db), {
    [`comments/${commentId}/${field}`]: value
  })
} 

export const deleteComment = (commentId) => {
  return update(ref(db), {
    [`comments/${commentId}`]: null,
  }) 
}
