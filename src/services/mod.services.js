import { get, ref, query, equalTo, orderByChild, update, push } from 'firebase/database';
import { db, storage } from '../config/firebase-config';
import {
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
  deleteObject,
  listAll,
  getMetadata,
  updateMetadata,
} from 'firebase/storage';
import { updateUserField } from './user.services';

export const fromModDocument = (snapshot, key) => {
  const mod = snapshot.val();

  return {
    ...mod,
    id: key,
    createdOn: new Date(mod.createdOn),
    images: mod.images ? Object.values(mod.images) : [],
    modInfo: mod.modInfo.split('|n|'),
    specifications: mod.specifications.split('|n|'),
    youtubeURL: mod.youtubeURL
      ? mod.youtubeURL
          .split('/')
          [mod.youtubeURL.split('/').length - 1].replace('v=', '')
          .replace('watch?', '')
      : null,
    youtubeURL_RAW: mod.youtubeURL,
    downloads: mod.downloads ? +mod.downloads : 0,
    versions: mod.versions ? Object.entries(mod.versions) : [],
    rating: mod.rating
      ? Object.values(mod.rating).reduce((acc, r) => acc + r, 0) / Object.keys(mod.rating).length
      : 0,
    totalNumberRatings: mod.rating ? Object.keys(mod.rating).length : 0,
    lastActivity: mod.lastActivity ? mod.lastActivity : {},
  };
};

export const getApprovedMods = () => {
  return get(query(ref(db, `mods`), orderByChild('approved'), equalTo(true)));
};

export const getModById = (modeId) => {
  return get(ref(db, `mods/${modeId}`)).then((snapshot) => {
    return fromModDocument(snapshot, modeId);
  });
};

export const getModsByUsername = async (username) => {
  return get(query(ref(db, 'mods'), orderByChild('author/username'), equalTo(username)));
};

export const getMaintainModsByUsername = async (username) => {
  return get(ref(db, `mods`)).then((snapshot) => {
    if (!snapshot.exists()) return [];
    return Object.entries(snapshot.val()).filter(
      ([id, mod]) => mod?.maintainer?.[username] === true
    );
  });
};

export const getAllMods = () => {
  return get(ref(db, `mods`))
    .then((snapshot) => snapshot)
    .catch(console.error);
};

export const addModBinaryDataToStorage = async (mod, modID) => {
  try {
    const mods = storageRef(storage, `mods/${modID}/${mod.name}`);

    const modSnapshot = await uploadBytes(mods, mod);

    const downloadURL = await getDownloadURL(modSnapshot.ref);

    await updateMetadata(storageRef(storage, `mods/${modID}/${mod.name}`), {
      customMetadata: { token: downloadURL.split('token=')[1] },
    });

    return downloadURL;
  } catch (e) {
    console.error(e);
  }
};

export const addModImageBinaryDataToStorage = async (image, modID, folderName) => {
  try {
    const mods = storageRef(storage, `mods/${modID}/${folderName}/${image.name}`);

    const imageSnapshot = await uploadBytes(mods, image);

    return await getDownloadURL(imageSnapshot.ref);
  } catch (e) {
    console.error(e);
  }
};

export const addModImage = (modID, value) => {
  return push(ref(db, `mods/${modID}/images`), value);
};

export const createMod = async (
  { uid, username, avatarUrl },
  title,
  mainCategory,
  subCategory,
  subSubCategory,
  modInfo,
  specifications,
  youtubeURL,
  isAdmin
) => {
  return push(ref(db, `mods`), {
    author: {
      uid,
      username,
      avatarUrl: avatarUrl || null,
    },
    approved: isAdmin ? true : null,
    title,
    mainCategory,
    subCategory,
    subSubCategory,
    modInfo,
    specifications,
    youtubeURL,
    createdOn: Number(new Date()),
    lastActivity: { [username]: Number(new Date()) },
    versions: {
      '1-0': 'First release',
    },
  });
};

export const updateModField = (modId, field, value) => {
  return update(ref(db), {
    [`mods/${modId}/${field}`]: value,
  });
};

export const deleteMainImageByModId = (mod) => {
  return listAll(storageRef(storage, `mods/${mod.id}/main-image`))
    .then(({ items }) => {
      for (const item of items)
        deleteObject(storageRef(storage, `mods/${mod.id}/main-image/${item.name}`));
    })
    .catch(console.error);
};

export const deleteSomeImagesByModId = async (mod, imageNamesArray) => {
  const deleted = [];
  try {
    const { items } = await listAll(storageRef(storage, `mods/${mod.id}/images`));
    for (const item of items)
      if (imageNamesArray.length === 0 || !imageNamesArray.includes(item.name)) {
        deleted.push(item.name);
        deleteObject(storageRef(storage, `mods/${mod.id}/images/${item.name}`)).catch(
          console.error()
        );
      }
  } catch (e) {
    console.error(e);
  }
  return deleted;
};

export const deleteModFileById = (modID, modToken) => {
  listAll(storageRef(storage, `mods/${modID}`))
    .then(async ({ items }) => {
      for (const item of items)
        if (!modToken) {
          deleteObject(storageRef(storage, `mods/${modID}/${item.name}`)).catch(console.error());
        } else {
          const {
            customMetadata: { token },
          } = await getMetadata(storageRef(storage, `mods/${modID}/${item.name}`));

          if (token === modToken)
            deleteObject(storageRef(storage, `mods/${modID}/${item.name}`)).catch(console.error());
        }
    })
    .catch(console.error);
};

export const deleteModById = (mod, username) => {
  deleteModFileById(mod.id);

  deleteMainImageByModId(mod);

  deleteSomeImagesByModId(mod, []);

  updateUserField(username, `mods/${mod.id}`, null).catch(console.error);
  return update(ref(db), {
    [`mods/${mod.id}`]: null,
  });
};
