# LoLMODS

## **Project Overview**

### **How to run the project:**

1. Clone the repository.
2. Run `npm install` in the terminal.
3. Then run `npm start` to start the application.

<br />

### **App info**

<img src="./public/LoLMODS.png" alt="logo" width="200"/>

* LoLMODS is a League of Legends mods web application. 

<br />

#### **Anonymous users**

* All users can browse and download their favorite mods.

<br />

#### **Registered user**

* The registered users can create their own and add other users as maintainer to help them manage their mods.
* The registered users also can commented and rate all the existing mods expect their own.

<br />

#### **Admin users**

* The admins approve the new mods. They also can edit/delete them, and ban other users.

<br />

#### **Mods**

* To create a mod you need to add title, category/s, information, specification, main image and the mod file itself. 
Also optionally up to 5 images and one YouTub link.
* After the mod is approved, the owner can add other users as maintainers.
* Owner and the maintainers can update all the information for the mod. They also can upload new version that should be approved from admin again.

<br />

### **Screenshots**

<br />
<img src="./public/screenshots/1.PNG" alt="1" width="600"/>
<br />
<img src="./public/screenshots/2.PNG" alt="2" width="600"/>
<br />
<img src="./public/screenshots/3.PNG" alt="3" width="600"/>

<br />

## **Technologies**

<br />

<img src="https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E" />
<img src="https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB" />
<img src="https://img.shields.io/badge/React_Router-CA4245?style=for-the-badge&logo=react-router&logoColor=white" />
<img src="https://img.shields.io/badge/firebase-ffca28?style=for-the-badge&logo=firebase&logoColor=black" />
<img src="https://img.shields.io/badge/Material%20UI-007FFF?style=for-the-badge&logo=mui&logoColor=white" />
<img src="https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white" />
<img src="https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white" />
<img src="https://img.shields.io/badge/Sass-CC6699?style=for-the-badge&logo=sass&logoColor=white" />



